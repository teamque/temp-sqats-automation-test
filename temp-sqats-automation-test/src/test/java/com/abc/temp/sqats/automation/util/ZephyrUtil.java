package com.abc.temp.sqats.automation.util;

import static com.jayway.restassured.RestAssured.given;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.jmeter.protocol.http.util.Base64Encoder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

/**
 * Class provides methods to perform functionalities with zephyr for jira
 * @author hmalick
 *
 */
public class ZephyrUtil {

	private static JSONParser parser = new JSONParser();
	private CloseableHttpClient httpclient = HttpClients.createDefault();
	private static String filePath = "X:\\git\\api-automation-test\\api-automation-test\\src\\test\\resources\\json\\";

	public static String zapiURL="https://agile.accuratebackground.com/rest/zapi/latest";
	public static String authUsername="test.automation";
	public static String authPassword="@utomat3";
	public static String projectName="AB API";
	public static String projectVersion="Sprint 8";
	/*public static String projectTestCycle="44";*/
	/*public static String projectTestCycle="43";*/
	/*public static String projectTestCycle="42";*/
	public static String projectTestCycle=null;
	public static Response testCaseResponse=null;
	public static Map<String, String> testCaseIdList=null;

	/**
	 * Method used to get all test cases for a given test cycle
	 */
	@BeforeTest
	public void getJiraTestList(){
		System.out.println("Zephyr Jira Test Cases List");
		System.out.println("================================================================================");
		projectTestCycle = System.getProperty("zephyrCycle");
		System.out.println("projectTestCycle[System.getProperty]: "+projectTestCycle);
		if (projectTestCycle==null) {
			projectTestCycle = ParameterDataSetup.testZephyrCycle;
		}
		System.out.println("projectTestCycle[Final]: "+projectTestCycle);
		testCaseResponse=getJiraTestCaseList(projectTestCycle);
		testCaseIdList=getJiraTestCaseIdList(testCaseResponse);		
		System.out.println("================================================================================");
	}

	/**
	 * Method used to execute test cases in testlink based on the passed or
	 * failed status of test/s. Provide test case id in zephyr for jira, status of the
	 * test and a comment
	 * 
	 * @param jiraTestCase
	 * @param status
	 * @param comment
	 */
	public void executeJiraTest(String jiraTestCase, String status, String comment) {
		//String projectId=getJiraProjectList();
		//String projectVersionId=getJiraProjectVersionList(projectId);
		//getJiraProjectCycleList(projectId, projectVersionId);

		//String testCaseId=getJiraTestCaseId(testCaseIdList, jiraTestCase);
		System.out.println("Execute Zephyr Jira Test Case");
		System.out.println("/******************************************************************************************************/");
		System.out.println("TestCaseJiraId: "+jiraTestCase);
		String testCaseId=testCaseIdList.get(jiraTestCase);
		Map<String, String> keyValueMap = new HashMap<String, String>();
		if (status.equals("pass")) {
			keyValueMap.put("status", "1");
			keyValueMap.put("comment", comment);
		} else if (status.equals("fail")){
			keyValueMap.put("status", "2");
			keyValueMap.put("comment", comment);
		} else {
			keyValueMap.put("status", "3");
			keyValueMap.put("comment", "skipped");
		}
		putJiraTestCaseExecution(testCaseId, changeJsonKeyValue("zephyr_test_execution.json", keyValueMap));
		System.out.println("/******************************************************************************************************/");

	}

	/**
	 * Method is used to get the zephyr for jira project list
	 * 
	 * @return
	 */
	public String getJiraProjectList(){
		String projectId=null;
		try {
			Response response = given().log().all().auth().preemptive().basic(authUsername, authPassword)
					.contentType("application/json").when()
					.get(zapiURL+"/util/project-list");
			System.out.println("Response: "+response.asString());


			int size = response.path("options.size()");  
			JsonPath jsonPath = new JsonPath(response.asString());
			for (int i=0; i < size; i++){
				if (jsonPath.getString("options.label["+i+"]").equals(projectName)){
					projectId=jsonPath.getString("options.value["+i+"]");
				}				
			}
			System.out.println("ProjectName: "+projectName);
			System.out.println("ProjectId: "+projectId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return projectId;
	}

	/**
	 * Method is used to get the zephyr for jira project version list
	 * 
	 * @param projectId
	 * @return
	 */
	public String getJiraProjectVersionList(String projectId){
		String projectVersionId=null;
		try {
			Response response = given().log().all().auth().preemptive().basic(authUsername, authPassword)
					.contentType("application/json").when().queryParam("projectId", projectId)
					.get(zapiURL+"/util/versionBoard-list");
			System.out.println("Response: "+response.asString());


			int size = response.path("unreleasedVersions.size()");  
			JsonPath jsonPath = new JsonPath(response.asString());
			for (int i=0; i < size; i++){
				if (jsonPath.getString("unreleasedVersions.label["+i+"]").equals(projectVersion)){
					projectVersionId=jsonPath.getString("unreleasedVersions.value["+i+"]");
				}				
			}
			System.out.println("ProjectVersion: "+projectVersion);
			System.out.println("ProjectVersionId: "+projectVersionId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return projectVersionId;
	}

	/**
	 * Method is used to get the zephyr for jira project cycle list
	 * 
	 * @param projectId
	 * @param projectVersionId
	 * @return
	 */
	public String getJiraProjectCycleList(String projectId, String projectVersionId){
		String projectCycleId=null;
		try {
			Response response = given().log().all().auth().preemptive().basic(authUsername, authPassword)
					.contentType("application/json").when().
					queryParam("versionId", projectVersionId).
					queryParam("projectId", projectId).
					get(zapiURL+"/cycle");
			System.out.println("Response: "+response.asString());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return projectVersionId;
	}

	public Response getJiraTestCaseList(String cycleId){
		Response testCaseList=null;
		try {
			Response response = given().log().all().auth().preemptive().basic(authUsername, authPassword)
					.contentType("application/json").when()
					.queryParam("cycleId", cycleId)
					.get(zapiURL+"/execution");
			System.out.println("Response: "+response.asString());
			testCaseList=response;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return testCaseList;
	}

	/*public String getJiraTestCaseId(Response response, String testCaseKey){
		String testCaseId=null;
		try {
			int size = response.path("executions.size()");  
			JsonPath jsonPath = new JsonPath(response.asString());
			for (int i=0; i < size; i++){
				if (jsonPath.getString("executions.issueKey["+i+"]").equals(testCaseKey)){
					testCaseId=jsonPath.getString("executions.id["+i+"]");
				}				
			}
			System.out.println("TestCaseJiraId: "+testCaseKey);
			System.out.println("TestCaseZephyrId: "+testCaseId);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return testCaseId;
	}*/

	public String getJiraTestCaseId(Map<String, String> testCaseList, String testCaseKey){
		String testCaseId=null;
		try {
			for (Map.Entry<String, String> element : testCaseList.entrySet()) {
				if (element.getKey().equals(testCaseKey)){
					testCaseId=element.getValue();
				}				
			}
			System.out.println("TestCaseJiraId: "+testCaseKey);
			System.out.println("TestCaseZephyrId: "+testCaseId);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return testCaseId;
	}

	public Map<String, String> getJiraTestCaseIdList(Response response){
		Map<String, String> testCaseIdList=new HashMap<String, String>();
		try {
			int size = response.path("executions.size()");  
			JsonPath jsonPath = new JsonPath(response.asString());
			for (int i=0; i < size; i++){
				testCaseIdList.put(jsonPath.getString("executions.issueKey["+i+"]"), jsonPath.getString("executions.id["+i+"]"));
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return testCaseIdList;
	}

	/*public Response putJiraTestCaseExecution(String testCaseId, String jsonRequest){
		Response testCaseList=null;
		try {
			Response response = given().log().all().auth().preemptive().basic(authUsername, authPassword)
					.contentType("application/json").body(jsonRequest).when()
					.put(zapiURL+"/execution/"+testCaseId+"/execute");
			System.out.println("Response: "+response.asString());
			response.then().statusCode(200);			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return testCaseList;
	}*/

	public Response putJiraTestCaseExecution(String testCaseId, String jsonRequest){
		Response testCaseList=null;
		try {
			String url=zapiURL+"/execution/"+testCaseId+"/execute";
			System.out.println("Url: "+url);
			RestTemplate restTemplate = new RestTemplate();
			String encodedCredentials= Base64Encoder.encode (authUsername+":"+authPassword);
			//HttpEntity<String> entity = new HttpEntity<String>(jsonRequest);
			//entity.getHeaders().add("Authorization", "Basic " + encodedCredentials);
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Authorization", "Basic "+encodedCredentials);

			RequestEntity<String> requestEntity = new RequestEntity<String>(jsonRequest, headers, HttpMethod.PUT, new URI(url));
			//requestEntity.getHeaders().add("Authorization", "Basic " + encodedCredentials);
			//restTemplate.put(url, entity);
			ResponseEntity<String> response = restTemplate.exchange(requestEntity, String.class);
			/*
			HttpPut request = HttpUtil.createPostRequestString(url, authUsername, 
					authPassword, jsonRequest);
			CloseableHttpResponse response = httpclient.execute(request);	
			*/			
			String jsonResponse = response.getBody();
			System.out.println("Response: "+jsonResponse);
			/*Assert.assertTrue(response.getStatusCode().equals(org.springframework.http.HttpStatus.OK));*/			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return testCaseList;
	}

	@SuppressWarnings("unchecked")
	public String changeJsonKeyValue(String filename, Map<String, String> keyValueMap){
		String updatedJson="";
		try {
			String originalJson = TestUtils.getFileJsonContentFromTestResource("zephyr/"+filename);
			JSONObject jsonObject =  (JSONObject) parser.parse(originalJson);
			//TODO - Improve for nested elements
			for(Map.Entry<String, String> entry : keyValueMap.entrySet()){
				jsonObject.put(entry.getKey(), entry.getValue());
			}
			//JSONObject nestedJsonObject = (JSONObject) jsonObject.get(key);

			updatedJson=jsonObject.toString();
			System.out.println("Updated Json Entity: "+updatedJson);
		} catch (ParseException e) {
			e.printStackTrace();
			/*Assert.fail("Fail", e);*/
		} catch (Exception e) {
			e.printStackTrace();
			/*Assert.fail("Fail", e);*/
		}
		return updatedJson;
	}

	public static String getFileJsonContentFromTestResource(String filename) throws Exception {
		return new String(Files.readAllBytes(Paths.get(filePath + "json/"+ filename)));
	}
}
