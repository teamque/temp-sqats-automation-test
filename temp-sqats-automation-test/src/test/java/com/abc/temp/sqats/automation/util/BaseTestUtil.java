package com.abc.temp.sqats.automation.util;

import java.lang.reflect.Method;
import java.net.MalformedURLException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

/**
 * Class is a base util class used for Selenium Test/s
 * 
 * @author hmalick
 *
 */
public abstract class BaseTestUtil {

	private String baseUrl;
	private MediaType baseMediaType = MediaType.APPLICATION_OCTET_STREAM;
	private static ZephyrUtil zephyrUtil = new ZephyrUtil();
	private static TestlinkUtil testlinkUtil = new TestlinkUtil();
	public WebDriver driver;
	private static final Logger log = LoggerFactory.getLogger(BaseTestUtil.class);
	public static String browser = null;

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * Method used to set driver for Selenium WebDriver
	 * 
	 * @return
	 */
	public WebDriver getDriver() {
		return driver;
	}

	/**
	 * Method helps to setup the browser before the Test/s. Provide the
	 * testbrowser such as chrome, firefox etc
	 * 
	 * @param testbrowser
	 * @throws MalformedURLException
	 */
	@Parameters("testbrowser")
	@BeforeClass
	public void beforeClass(String testbrowser) throws MalformedURLException {
		log.info("Starting Test Class");
		SetBrowser set = new SetBrowser();
		if (ParameterDataSetup.testSetBrowser.equals("yes")) {
			driver = set.setBrowser(testbrowser);
			browser = testbrowser;
		} else {
			driver = set.setBrowser(ParameterDataSetup.testBrowser);
			browser = ParameterDataSetup.testBrowser;
		}
		driver.manage().window().maximize();
	}

	/**
	 * Method helps tear down the browser after the Test/s are complete
	 */
	@AfterClass
	public void afterClass() {
		try {
			log.info("Finished Test Class");
			driver.quit();

		} catch (Exception ex) {
			log.error("Failed", ex);
		}
	}

	/**
	 * Method is used to print test method header before a Test method is run
	 * 
	 * @param testbrowser
	 * @param method
	 * @throws MalformedURLException
	 */
	@Parameters("testbrowser")
	@BeforeMethod
	public void beforeMethod(String testbrowser, Method method) throws MalformedURLException {
		printHeader(method);

		log.info("Starting Test Case: " + method.getName());
	}

	/**
	 * Method is used to print test method footer and use the TestNG result to
	 * execute the test case in Testlink after a Test method is run
	 * 
	 * @param method
	 * @param result
	 */
	@AfterMethod
	public void afterMethod(Method method, ITestResult result) {
		try {
			log.info("Finished Test Case: " + method.getName());
			printFooter(method);
			// executeZephyrTest(result, method); //enable to update zephyr tests
			executeTestlinkTest(result, method);
		} catch (Exception ex) {
			printFooter(method);
			// executeZephyrTest(result, method);
			executeTestlinkTest(result, method);
		}
	}

	/**
	 * Method is used to execute test case in Testlink using the TestNG result
	 * 
	 * @param result
	 * @param method
	 */
	public void executeTestlinkTest(ITestResult result, Method method) {
		if (result.getStatus() == ITestResult.FAILURE) {
			TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
			String jira = null;
			if (null != testMetadata) {
				jira = testMetadata.testcase();
				if (null == jira) {
					jira = "JIRA-#: not defined. Use @TestMetadata";
				}
			}
			testlinkUtil.executeTestlinkTest(jira, "fail", "failed: " + result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
			String jira = null;
			if (null != testMetadata) {
				jira = testMetadata.testcase();
				if (null == jira) {
					jira = "JIRA-#: not defined. Use @TestMetadata";
				}
			}
			testlinkUtil.executeTestlinkTest(jira, "pass", "passed");
		}
	}

	/**
	 * Method is used to execute test case in Zephyr using the TestNG result
	 * 
	 * @param result
	 * @param method
	 */
	public void executeZephyrTest(ITestResult result, Method method) {
		if (result.getStatus() == ITestResult.FAILURE) {
			TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
			String jira = null;
			if (null != testMetadata) {
				jira = testMetadata.testcase();
				if (null == jira) {
					jira = "JIRA-#: not defined. Use @TestMetadata";
				}
			}
			/*
			 * zephyrUtil.executeJiraTest(jira, "fail",
			 * "failed: "+result.getThrowable().getMessage().substring(0, 50));
			 */
			zephyrUtil.executeJiraTest(jira, "fail", "failed");
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
			String jira = null;
			if (null != testMetadata) {
				jira = testMetadata.testcase();
				if (null == jira) {
					jira = "JIRA-#: not defined. Use @TestMetadata";
				}
			}
			zephyrUtil.executeJiraTest(jira, "pass", "passed");
		}

	}

	/**
	 * Method used to print header of a Test
	 * 
	 * @param method
	 */
	public void printHeader(Method method) {
		TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
		String jira = null;
		String title = null;
		if (null != testMetadata) {
			jira = testMetadata.testcase();
			title = StringUtils.stripToNull(testMetadata.title());
			if (null == title) {
				title = method.getName();
			}
			if (null == jira) {
				jira = "JIRA-#: not defined. Use @TestMetadata";
			}
			printHeader(jira, title);
		}
	}

	/**
	 * Method used to print footer of a Test
	 * 
	 * @param method
	 */
	public void printFooter(Method method) {
		TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
		String title = null;
		if (null != testMetadata) {
			title = StringUtils.stripToNull(testMetadata.title());
			if (null == title) {
				title = method.getName();
			}
			printFooter(title);
		}
	}

	public void printHeader(String jira, String title) {
		System.out.println("");
		System.out.println(
				"/*####################################################################################################*/");
		System.out.println("  Started Test Case...");
		System.out.println("  " + jira);
		System.out.println("  " + title);
		System.out.println(
				"/*####################################################################################################*/");
	}

	public void printFooter(String title) {
		System.out.println();
		System.out.println("  Finished Test Case: " + title);
		System.out.println();
		System.out.println(
				"/******************************************************************************************************/");
		System.out.println("");
	}

}
