package com.abc.temp.sqats.automation.util;

import java.net.MalformedURLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.jayway.restassured.path.json.JsonPath;

/**
 * Class provides methods to setup the parameter data used for the Test/s
 * 
 * @author hmalick
 *
 */
public class ParameterDataSetup {

	private static final Logger log = LoggerFactory.getLogger(ParameterDataSetup.class);

	public static String testEnvironment;
	public static String testGridServer;
	public static String testBrowser;
	public static String dbconnURL = "";
	public static String dbconnUsername = "";
	public static String dbconnPassword = "";
	public static String dbconnDatabase = "";

	public static String testSetBrowser = "";
	public static String testScreenShotPath = "";
	public static String testEnvironmentName = "";

	public static String testZephyrCycle = "";
	public static String testFilePath = "";
	public static String testDataFilePath = "";
	public static JsonPath testParameters = null;
	public static String mongodbconnURI = null;
	public static String testTestlinkUrl = "";
	public static String testTestlinkDevKey = "";
	public static String testTestlinkTestProject = "";
	public static String testTestlinkTestPlan = "";

	/**
	 * Method used to setup the parameter data for the Test/s. Provide the
	 * following parameters
	 * 
	 * @param environment
	 * @param gridserver
	 * @param browser
	 * @param dbconnURL_DEV
	 * @param dbconnUsername_DEV
	 * @param dbconnPassword_DEV
	 * @param dbconnDatabase_DEV
	 * @param setbrowser
	 * @param screenshotPath
	 * @param JsonFilePath
	 * @param zephyrCycle
	 * @param testlinkUrl
	 * @param testlinkDevKey
	 * @param testlinkTestProject
	 * @param testlinkTestPlan
	 * @throws Exception
	 */
	@Parameters({ "environment", "gridserver", "browser", "dbconnURL_DEV", "dbconnUsername_DEV", "dbconnPassword_DEV",
			"dbconnDatabase_DEV", "setbrowser", "screenshotPath", "jsonfilePath", "zephyrCycle", "testlinkUrl",
			"testlinkDevKey", "testlinkTestProject", "testlinkTestPlan" })

	@BeforeTest
	public static void setupData(String environment, String gridserver, String browser, String dbconnURL_DEV,
			String dbconnUsername_DEV, String dbconnPassword_DEV, String dbconnDatabase_DEV, String setbrowser,
			String screenshotPath, String JsonFilePath, String zephyrCycle, String testlinkUrl, String testlinkDevKey,
			String testlinkTestProject, String testlinkTestPlan) throws Exception {

		// Get testEnvironment
		testEnvironment = System.getProperty("environment");

		log.info("testEnvironment[System.getProperty]: " + testEnvironment);
		if (testEnvironment == null) {
			testEnvironment = environment;
		}
		log.info("testEnvironment[Final]: " + testEnvironment);

		testGridServer = gridserver;
		testBrowser = browser;
		testSetBrowser = setbrowser;
		testScreenShotPath = screenshotPath;
		testTestlinkUrl = testlinkUrl;
		testTestlinkDevKey = testlinkDevKey;
		testTestlinkTestProject = testlinkTestProject;
		
		log.info("testTestlinkUrl: " + testTestlinkUrl);
		log.info("testTestlinkDevKey: " + testTestlinkDevKey);
		log.info("testTestlinkTestProject: " + testTestlinkTestProject);
		
		// Get testlinkTestPlan
		testTestlinkTestPlan = System.getProperty("testlinkPlan");

		log.info("testTestlinkTestPlan[System.getProperty]: " + testTestlinkTestPlan);
		if (testTestlinkTestPlan == null) {
			testTestlinkTestPlan = testlinkTestPlan;
		}
		log.info("testTestlinkTestPlan[Final]: " + testTestlinkTestPlan);

		// Get zephyrCycleId
		testZephyrCycle = System.getProperty("zephyrCycle");

		log.info("zephyrCycleId[System.getProperty]: " + testZephyrCycle);
		if (testZephyrCycle == null) {
			testZephyrCycle = zephyrCycle;
		}

		// Get json path
		testFilePath = System.getProperty("jsonfilepath");
		log.info("testFilePath[System.getProperty]: " + testFilePath);
		if (testFilePath == null) {
			testFilePath = "src/test/resources/";
		}
		log.info("testFilePath[Final]: " + testFilePath);

		if (testEnvironment.equals("LOC")) {
			testEnvironmentName = "LOCAL";
		} else if (testEnvironment.equals("DEV")) {
			testEnvironmentName = "DEV";
		} else if (testEnvironment.equals("QA")) {
			testEnvironmentName = "QA";
		} else if (testEnvironment.equals("UAT")) {
			testEnvironmentName = "UAT";
		} else if (testEnvironment.equals("PRD")) {
			testEnvironmentName = "Production";
		}

		log.info("Running Tests in: " + testEnvironmentName + " environment \n");

		if (testEnvironment.equals("LOC")) {
			dbconnURL = dbconnURL_DEV;
			dbconnUsername = dbconnUsername_DEV;
			dbconnPassword = dbconnPassword_DEV;
			dbconnDatabase = dbconnDatabase_DEV;
			log.info("Connecting to " + testEnvironmentName + " database: " + dbconnURL + ", " + dbconnUsername + ", "
					+ dbconnPassword);

		} else if (testEnvironment.equals("DEV")) {
			dbconnURL = dbconnURL_DEV;
			dbconnUsername = dbconnUsername_DEV;
			dbconnPassword = dbconnPassword_DEV;
			dbconnDatabase = dbconnDatabase_DEV;
			testDataFilePath = "testdata/dev/";
			testParameters = FileHandlerUtil
					.getFileJsonPathFromTestResource(testDataFilePath + "config/properties.json");

			log.info("Connecting to " + testEnvironmentName + " database: " + dbconnURL + ", " + dbconnUsername + ", "
					+ dbconnPassword);

		} else if (testEnvironment.equals("QA")) {
			/*
			 * dbconnURL = dbconnURL_QA; dbconnUsername = dbconnUsername_QA;
			 * dbconnPassword = dbconnPassword_QA; dbconnDatabase =
			 * dbconnDatabase_QA;
			 */
			testDataFilePath = "testdata/qa/";
			testParameters = FileHandlerUtil
					.getFileJsonPathFromTestResource(testDataFilePath + "config/properties.json");

			log.info("Connecting to " + testEnvironmentName + " database: " + dbconnURL + ", " + dbconnUsername + ", "
					+ dbconnPassword);

		} else if (testEnvironment.equals("UAT")) {
			/*
			 * dbconnURL = dbconnURL_UAT; dbconnUsername = dbconnUsername_UAT;
			 * dbconnPassword = dbconnPassword_UAT; dbconnDatabase =
			 * dbconnDatabase_UAT;
			 */
			log.info("Connecting to " + testEnvironmentName + " database: " + dbconnURL + ", " + dbconnUsername + ", "
					+ dbconnPassword);

		} else if (testEnvironment.equals("PRD")) {
			/*
			 * dbconnURL = dbconnURL_PRD; dbconnUsername = dbconnUsername_PRD;
			 * dbconnPassword = dbconnPassword_PRD; dbconnDatabase =
			 * dbconnDatabase_PRD;
			 */
			log.info("Connecting to " + testEnvironmentName + " database: " + dbconnURL + ", " + dbconnUsername + ", "
					+ dbconnPassword);

		}

	}

}
