package com.abc.temp.sqats.automation.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

/**
 * Class is a base util class used for RESTful API Test/s
 * 
 * @author hmalick
 *
 */
public abstract class APIBaseTestUtil {

	private Logger log;
	private String baseUrl;
	private MediaType baseMediaType = MediaType.APPLICATION_OCTET_STREAM;
	private static ZephyrUtil zephyrUtil = new ZephyrUtil();
	private static TestlinkUtil testlinkUtil = new TestlinkUtil();

	/**
	 * Method gets the url for RESTful API
	 * 
	 * @return
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * Method sets the url for RESTful API
	 * 
	 * @param baseUrl
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public APIBaseTestUtil(Logger log) {
		this.log = log;
	}

	public APIBaseTestUtil(Logger log, String baseUrl) {
		this.log = log;
		this.baseUrl = baseUrl;
	}

	/**
	 * Method sets the configuration for a specific micro service using the
	 * RESTful API
	 * 
	 * @param mic
	 */
	public void setMicConfiguration(MicConfiguration mic) {
		RestAssured.baseURI = mic.getMicDomain();
		if (null != mic.getMicPort()) {
			RestAssured.port = mic.getMicPort();
		}
	}

	/**
	 * Method is used to print test method header before a Test method is run
	 * 
	 * @param method
	 */
	@BeforeMethod
	public void beforeMethod(Method method) {
		printHeader(method);
	}

	/**
	 * Method is used to print test method footer and use the TestNG result to
	 * execute the test case in Testlink after a Test method is run
	 * 
	 * @param method
	 * @param result
	 */
	@AfterMethod
	public void afterMethod(Method method, ITestResult result) {
		printFooter(method);
		// executeZephyrTest(result, method); //enable to update zephyr tests
		executeTestlinkTest(result, method);

	}

	/**
	 * Method is used to execute test case in Zephyr using the TestNG result
	 * 
	 * @param result
	 * @param method
	 */
	public void executeZephyrTest(ITestResult result, Method method) {
		if (result.getStatus() == ITestResult.FAILURE) {
			TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
			String jira = null;
			if (null != testMetadata) {
				jira = testMetadata.testcase();
				if (null == jira) {
					jira = "JIRA-#: not defined. Use @TestMetadata";
				}
			}
			zephyrUtil.executeJiraTest(jira, "fail", "failed: " + result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
			String jira = null;
			if (null != testMetadata) {
				jira = testMetadata.testcase();
				if (null == jira) {
					jira = "JIRA-#: not defined. Use @TestMetadata";
				}
			}
			zephyrUtil.executeJiraTest(jira, "pass", "passed");
		}
	}

	/**
	 * Method is used to execute test case in Testlink using the TestNG result
	 * 
	 * @param result
	 * @param method
	 */
	public void executeTestlinkTest(ITestResult result, Method method) {
		if (result.getStatus() == ITestResult.FAILURE) {
			TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
			String jira = null;
			if (null != testMetadata) {
				jira = testMetadata.testcase();
				if (null == jira) {
					jira = "JIRA-#: not defined. Use @TestMetadata";
				}
			}
			testlinkUtil.executeTestlinkTest(jira, "fail", "failed: " + result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
			String jira = null;
			if (null != testMetadata) {
				jira = testMetadata.testcase();
				if (null == jira) {
					jira = "JIRA-#: not defined. Use @TestMetadata";
				}
			}
			testlinkUtil.executeTestlinkTest(jira, "pass", "passed");
		}
	}

	/**
	 * Method used to print header of a Test
	 * 
	 * @param method
	 */
	public void printHeader(Method method) {
		TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
		String jira = null;
		String title = null;
		if (null != testMetadata) {
			jira = testMetadata.testcase();
			title = StringUtils.stripToNull(testMetadata.title());

			if (null == title) {
				title = method.getName();
			}
			if (null == jira) {
				jira = "JIRA-#: not defined. Use @TestMetadata";
			}
			printHeader(jira, title);
		}
	}

	/**
	 * Method used to print footer of a Test
	 * 
	 * @param method
	 */
	public void printFooter(Method method) {
		TestMetaData testMetadata = method.getAnnotation(TestMetaData.class);
		String title = null;
		if (null != testMetadata) {
			title = StringUtils.stripToNull(testMetadata.title());
			if (null == title) {
				title = method.getName();
			}
			printFooter(title);
		}
	}

	public void printHeader(String jira, String title) {
		System.out.println("");
		System.out.println(
				"/*####################################################################################################*/");
		System.out.println("  Started Test Case...");
		System.out.println("  " + jira);
		System.out.println("  " + title);
		System.out.println(
				"/*####################################################################################################*/");
	}

	public void printFooter(String title) {
		System.out.println();
		System.out.println("  Finished Test Case: " + title);
		System.out.println();
		System.out.println(
				"/******************************************************************************************************/");
		System.out.println("");
	}

	/**
	 * Method used to check the status of a micro service using RESTful APIs
	 * 
	 * @param microservice
	 */
	public void checkMicroservices(APIMiceroservicesEnum microservice) {
		MicroserviceAliveChecker checker = new MicroserviceAliveChecker();
		checker.addMiceroservice(microservice);
		boolean isAlive = checker.check();
		checker.print();
		Assert.assertTrue(isAlive, "Microservice NOT live: " + checker.getMicroservicesAlive());
	}

	public void checkMicroservices(List<APIMiceroservicesEnum> microservices) {
		MicroserviceAliveChecker checker = new MicroserviceAliveChecker();
		checker.setMicroservices(microservices);
		boolean isAlive = checker.check();
		checker.print();
		Assert.assertTrue(isAlive, "Microservice NOT live: " + checker.getMicroservicesAlive());
	}

	public Response runTests(HttpMethod httpMethod, MediaType mediaType, String filename) {
		return runTests(null, null, httpMethod, mediaType, filename, null);
	}

	public Response runTests(String url, HttpMethod httpMethod, MediaType mediaType, String filename) {
		return runTests(url, null, null, httpMethod, mediaType, filename, null);
	}

	public Response runTests(String jira, String title, HttpMethod httpMethod, MediaType mediaType, String filename) {
		return runTests(jira, title, httpMethod, mediaType, filename, null);
	}

	public Response runTests(String url, String jira, String title, HttpMethod httpMethod) {
		return runTests(url, jira, title, httpMethod, baseMediaType, null);
	}

	public Response runTests(String url, String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename) {
		return runTests(url, jira, title, httpMethod, mediaType, filename, null);
	}

	public Response runTests(String jira, String title, HttpMethod httpMethod, MediaType mediaType, String filename,
			String validationMethod) {
		return runTests(baseUrl, jira, title, httpMethod, mediaType, filename, validationMethod);
	}

	public Response runTestsWithHeaders(String url, String jira, String title, HttpMethod httpMethod,
			Map<String, Object> header) {
		return runTestsWithHeaders(url, jira, title, httpMethod, baseMediaType, null, null, header, null);
	}

	public Response runTestsWithHeaders(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, Map<String, Object> header) {
		return runTestsWithHeaders(jira, title, httpMethod, mediaType, filename, null, header);
	}

	public Response runTestsWithHeaders(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, Map<String, Object> header) {
		return runTestsWithHeaders(url, jira, title, httpMethod, mediaType, filename, null, header, null);
	}

	public Response runTestsWithHeaders(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, String validationMethod, Map<String, Object> header) {
		return runTestsWithHeaders(baseUrl, jira, title, httpMethod, mediaType, filename, validationMethod, header,
				null);
	}

	public Response runTestsWithHeaders(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, String validationMethod, Map<String, Object> header, String jsonRequestString) {
		return runTestsWithHeaders(baseUrl, jira, title, httpMethod, mediaType, filename, validationMethod, header,
				jsonRequestString);
	}

	/**
	 * Method used to execute RESTful API calls such as POST, GET, PUT, PATCH,
	 * DELETE using RESTAssured libraries using Http Headers
	 * 
	 * @param url
	 * @param jira
	 * @param title
	 * @param httpMethod
	 * @param mediaType
	 * @param filename
	 * @param validationMethod
	 * @param header
	 * @param jsonRequestString
	 * @return
	 */
	public Response runTestsWithHeaders(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, String validationMethod, Map<String, Object> header,
			String jsonRequestString) {
		Response response = null;
		try {
			log.info("");
			if (null != title) {
				printHeader(jira, title);
			}
			System.out.println();
			System.out.println("  HttpMethod: " + httpMethod.toString());
			System.out.println("  MediaType:  " + mediaType.toString());
			System.out.println("  Filename:   " + filename);
			System.out.println();
			// "/v1/order"
			response = TestUtils.run(url, // baseUrl
					httpMethod, mediaType, filename, header, jsonRequestString);

			if (null != validationMethod) {
				callValidationMethod(response, validationMethod);
			}

		} catch (Exception e) {
			log.error("Fail", e);
			if (null != e.getCause()) {
				Assert.fail("Fail " + e.getCause().getMessage(), e);
			} else {
				Assert.fail("Fail " + e.getMessage(), e);
			}
		} finally {
			if (null != title) {
				printFooter(title);
			}
			System.out.println();
		}
		return response;
	}

	/**
	 * Method used to execute RESTful API calls such as POST, GET, PUT, PATCH,
	 * DELETE using RESTAssured libraries
	 * 
	 * @param url
	 * @param jira
	 * @param title
	 * @param httpMethod
	 * @param mediaType
	 * @param filename
	 * @param validationMethod
	 * @return
	 */
	public Response runTests(String url, String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, String validationMethod) {
		Response response = null;
		try {
			log.info("");
			if (null != title) {
				printHeader(jira, title);
			}
			System.out.println();
			System.out.println("  HttpMethod: " + httpMethod.toString());
			System.out.println("  MediaType:  " + mediaType.toString());
			System.out.println("  Filename:   " + filename);
			System.out.println();
			// "/v1/order"
			response = TestUtils.run(url, // baseUrl
					httpMethod, mediaType, filename);

			if (null != validationMethod) {
				callValidationMethod(response, validationMethod);
			}

		} catch (Exception e) {
			log.error("Fail", e);
			if (null != e.getCause()) {
				Assert.fail("Fail " + e.getCause().getMessage(), e);
			} else {
				Assert.fail("Fail " + e.getMessage(), e);
			}
		} finally {
			if (null != title) {
				printFooter(title);
			}
			System.out.println();
		}
		return response;
	}

	public Response runTestsWithRequestString(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename) {
		return runTestsWithRequestString(baseUrl, jira, title, httpMethod, mediaType, filename, null, null, null);
	}

	public Response runTestsWithRequestString(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, String jsonRequestString) {
		return runTestsWithRequestString(baseUrl, jira, title, httpMethod, mediaType, filename, null, null,
				jsonRequestString);
	}

	public Response runTestsWithRequestString(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, String validationMethod, String jsonRequestString) {
		return runTestsWithRequestString(baseUrl, jira, title, httpMethod, mediaType, filename, validationMethod, null,
				jsonRequestString);
	}

	public Response runTestsWithRequestString(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, String jsonRequestString) {
		return runTestsWithRequestString(url, jira, title, httpMethod, mediaType, filename, null, null,
				jsonRequestString);
	}

	public Response runTestsWithRequestString(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, Map<String, Object> header, String jsonRequestString) {
		return runTestsWithRequestString(baseUrl, jira, title, httpMethod, mediaType, filename, null, header,
				jsonRequestString);
	}

	public Response runTestsWithRequestString(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, Map<String, Object> header, String jsonRequestString) {
		return runTestsWithRequestString(url, jira, title, httpMethod, mediaType, filename, null, header,
				jsonRequestString);
	}

	public Response runTestsWithRequestString(String url, HttpMethod httpMethod, MediaType mediaType, String filename,
			Map<String, Object> header) {
		return runTestsWithRequestString(url, null, null, httpMethod, mediaType, filename, header);
	}

	public Response runTestsWithRequestString(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, Map<String, Object> header) {
		return runTestsWithRequestString(url, jira, title, httpMethod, mediaType, filename, null, header, null);
	}

	/**
	 * Method used to execute RESTful API calls such as POST, GET, PUT, PATCH,
	 * DELETE using RESTAssured libraries using a JSON request string
	 * 
	 * @param url
	 * @param jira
	 * @param title
	 * @param httpMethod
	 * @param mediaType
	 * @param filename
	 * @param validationMethod
	 * @param header
	 * @param jsonRequestString
	 * @return
	 */
	public Response runTestsWithRequestString(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, String validationMethod, Map<String, Object> header,
			String jsonRequestString) {
		Response response = null;
		try {
			log.info("");
			if (null != title) {
				printHeader(jira, title);
			}
			System.out.println();
			System.out.println("  HttpMethod: " + httpMethod.toString());
			System.out.println("  MediaType:  " + mediaType.toString());
			System.out.println("  Filename:   " + filename);
			System.out.println();
			// "/v1/order"
			response = TestUtils.run(url, // baseUrl
					httpMethod, mediaType, filename, header, jsonRequestString);

			if (null != validationMethod) {
				callValidationMethod(response, validationMethod);
			}

		} catch (Exception e) {
			log.error("Fail", e);
			if (null != e.getCause()) {
				Assert.fail("Fail " + e.getCause().getMessage(), e);
			} else {
				Assert.fail("Fail " + e.getMessage(), e);
			}
		} finally {
			if (null != title) {
				printFooter(title);
			}
			System.out.println();
		}
		return response;
	}

	public Response runTestsWithDuplicateCheck(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, Map<String, String> duplicateCheck) {
		return runTestsWithDuplicateCheck(baseUrl, jira, title, httpMethod, mediaType, filename, null, null, null,
				duplicateCheck);
	}

	public Response runTestsWithDuplicateCheck(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, String validationMethod, Map<String, String> duplicateCheck) {
		return runTestsWithDuplicateCheck(baseUrl, jira, title, httpMethod, mediaType, filename, validationMethod, null,
				null, duplicateCheck);
	}

	public Response runTestsWithDuplicateCheck(String url, HttpMethod httpMethod, MediaType mediaType, String filename,
			Map<String, Object> header, Map<String, String> duplicateCheck) {
		return runTestsWithDuplicateCheck(url, null, null, httpMethod, mediaType, filename, header, duplicateCheck);
	}

	public Response runTestsWithDuplicateCheck(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, Map<String, Object> header, Map<String, String> duplicateCheck) {
		return runTestsWithDuplicateCheck(baseUrl, jira, title, httpMethod, mediaType, filename, null, header, null,
				duplicateCheck);
	}

	public Response runTestsWithDuplicateCheck(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, Map<String, Object> header, Map<String, String> duplicateCheck) {
		return runTestsWithDuplicateCheck(url, jira, title, httpMethod, mediaType, filename, null, header, null,
				duplicateCheck);
	}

	public Response runTestsWithDuplicateCheck(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, Map<String, Object> header, String jsonRequestString, Map<String, String> duplicateCheck) {
		return runTestsWithDuplicateCheck(baseUrl, jira, title, httpMethod, mediaType, filename, null, header,
				jsonRequestString, duplicateCheck);
	}

	public Response runTestsWithDuplicateCheck(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, String jsonRequestString, Map<String, String> duplicateCheck) {
		return runTestsWithDuplicateCheck(url, jira, title, httpMethod, mediaType, filename, null, null,
				jsonRequestString, duplicateCheck);
	}

	public Response runTestsWithDuplicateCheck(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, Map<String, Object> header, String jsonRequestString,
			Map<String, String> duplicateCheck) {
		return runTestsWithDuplicateCheck(url, jira, title, httpMethod, mediaType, filename, null, header,
				jsonRequestString, duplicateCheck);
	}

	/**
	 * Method used to execute RESTful API calls such as POST, GET, PUT, PATCH,
	 * DELETE using RESTAssured libraries using a JSON request string or a
	 * duplicate check Query parameter
	 * 
	 * @param url
	 * @param jira
	 * @param title
	 * @param httpMethod
	 * @param mediaType
	 * @param filename
	 * @param validationMethod
	 * @param header
	 * @param jsonRequestString
	 * @param duplicateCheck
	 * @return
	 */
	public Response runTestsWithDuplicateCheck(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, String validationMethod, Map<String, Object> header,
			String jsonRequestString, Map<String, String> duplicateCheck) {
		Response response = null;
		try {
			log.info("");
			if (null != title) {
				printHeader(jira, title);
			}
			System.out.println();
			System.out.println("  HttpMethod: " + httpMethod.toString());
			System.out.println("  MediaType:  " + mediaType.toString());
			System.out.println("  Filename:   " + filename);
			System.out.println();
			// "/v1/order"
			response = TestUtils.run(url, // baseUrl
					httpMethod, mediaType, filename, header, jsonRequestString, duplicateCheck);

			if (null != validationMethod) {
				callValidationMethod(response, validationMethod);
			}

		} catch (Exception e) {
			log.error("Fail", e);
			if (null != e.getCause()) {
				Assert.fail("Fail " + e.getCause().getMessage(), e);
			} else {
				Assert.fail("Fail " + e.getMessage(), e);
			}
		} finally {
			if (null != title) {
				printFooter(title);
			}
			System.out.println();
		}
		return response;
	}

	public Response runTestsWithBasicAuth(String url, String jira, String title, HttpMethod httpMethod,
			Map<String, String> basicAuth) {
		return runTestsWithBasicAuth(url, jira, title, httpMethod, baseMediaType, null, null, null, null, null,
				basicAuth);
	}

	public Response runTestsWithBasicAuth(String jira, String title, HttpMethod httpMethod, MediaType mediaType,
			String filename, String jsonRequestString, Map<String, String> duplicateCheck,
			Map<String, String> basicAuth) {
		return runTestsWithBasicAuth(baseUrl, jira, title, httpMethod, mediaType, filename, null, null,
				jsonRequestString, duplicateCheck, basicAuth);
	}

	public Response runTestsWithBasicAuth(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, String jsonRequestString, Map<String, String> basicAuth) {
		return runTestsWithBasicAuth(url, jira, title, httpMethod, mediaType, filename, null, null, jsonRequestString,
				basicAuth);
	}

	public Response runTestsWithBasicAuth(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, String validationMethod, Map<String, Object> header,
			String jsonRequestString, Map<String, String> basicAuth) {
		return runTestsWithBasicAuth(url, jira, title, httpMethod, mediaType, filename, null, null, jsonRequestString,
				null, basicAuth);
	}

	/**
	 * Method used to execute RESTful API calls such as POST, GET, PUT, PATCH,
	 * DELETE using RESTAssured libraries using Basic authentication
	 * 
	 * @param url
	 * @param jira
	 * @param title
	 * @param httpMethod
	 * @param mediaType
	 * @param filename
	 * @param validationMethod
	 * @param header
	 * @param jsonRequestString
	 * @param duplicateCheck
	 * @param basicAuth
	 * @return
	 */
	public Response runTestsWithBasicAuth(String url, String jira, String title, HttpMethod httpMethod,
			MediaType mediaType, String filename, String validationMethod, Map<String, Object> header,
			String jsonRequestString, Map<String, String> duplicateCheck, Map<String, String> basicAuth) {
		Response response = null;
		try {
			log.info("");
			if (null != title) {
				printHeader(jira, title);
			}
			System.out.println();
			System.out.println("  HttpMethod: " + httpMethod.toString());
			System.out.println("  MediaType:  " + mediaType.toString());
			System.out.println("  Filename:   " + filename);
			System.out.println();
			// "/v1/order"
			response = TestUtils.run(url, // baseUrl
					httpMethod, mediaType, filename, header, jsonRequestString, duplicateCheck, basicAuth);

			if (null != validationMethod) {
				callValidationMethod(response, validationMethod);
			}

		} catch (Exception e) {
			log.error("Fail", e);
			if (null != e.getCause()) {
				Assert.fail("Fail " + e.getCause().getMessage(), e);
			} else {
				Assert.fail("Fail " + e.getMessage(), e);
			}
		} finally {
			if (null != title) {
				printFooter(title);
			}
			System.out.println();
		}
		return response;
	}

	/**
	 * Method used to validate JSON response
	 * 
	 * @param response
	 * @param validationMethod
	 * @throws Exception
	 */
	public void callValidationMethod(Response response, String validationMethod) throws Exception {
		Method method = this.getClass().getDeclaredMethod(validationMethod, Response.class);
		method.invoke(this, response);
	}

	public void validate(JsonPath jsonMongoDB, JsonPath jsonResponse, String path) throws Exception {
		validate(jsonMongoDB, jsonResponse, path, null);
	}

	/**
	 * Method used to validate JSON response with MongoDB response
	 * 
	 * @param jsonMongoDB
	 * @param jsonResponse
	 * @param path
	 * @param prefix
	 * @throws Exception
	 */
	public void validate(JsonPath jsonMongoDB, JsonPath jsonResponse, String path, String prefix) throws Exception {
		if (null != prefix) {
			Assert.assertEquals(jsonMongoDB.getString(path), jsonResponse.getString(prefix + "." + path),
					"validate: " + path);
		} else {
			Assert.assertEquals(jsonMongoDB.getString(path), jsonResponse.getString(path), "validate: " + path);
		}
	}

	public boolean validateExists(JsonPath json, String path) throws Exception {
		return null != json.getString(path);
	}

	/**
	 * Method used to validate JSON response exists in MongoDB
	 * 
	 * @param jsonMongoDB
	 * @param jsonResponse
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public boolean validateExists(JsonPath jsonMongoDB, JsonPath jsonResponse, String path) throws Exception {
		boolean mongo = null != jsonMongoDB.getString(path);
		boolean resp = null != jsonResponse.getString(path);
		if (mongo == resp) {
			return mongo && resp;
		} else {
			Assert.assertNotEquals(mongo, resp, "validateExists: " + path);
			return false;
		}
	}

	public void validateListCount(JsonPath jsonMongoDB, JsonPath jsonResponse, String path) throws Exception {
		validateListCount(jsonMongoDB, jsonResponse, path, null);
	}

	/**
	 * Method used to validate JSON response list with MongoDB response list
	 * 
	 * @param jsonMongoDB
	 * @param jsonResponse
	 * @param path
	 * @param prefix
	 * @throws Exception
	 */
	public void validateListCount(JsonPath jsonMongoDB, JsonPath jsonResponse, String path, String prefix)
			throws Exception {
		if (null != prefix) {
			List<Object> listResponse = jsonResponse.getList(prefix + "." + path);
			List<Object> listMongoDB = jsonMongoDB.getList(path);
			Assert.assertEquals(listMongoDB.size(), listResponse.size(), "validateListCount: " + path);
		} else {
			List<Object> listResponse = jsonResponse.getList(path);
			List<Object> listMongoDB = jsonMongoDB.getList(path);
			Assert.assertEquals(listMongoDB.size(), listResponse.size(), "validateListCount: " + path);
		}
	}

	/**
	 * Method used to validate date in JSON response
	 * 
	 * @param jsonMongoDB
	 * @param jsonResponse
	 * @param path
	 * @throws Exception
	 */
	public void validateDate(JsonPath jsonMongoDB, JsonPath jsonResponse, String path) throws Exception {
		if (null == jsonResponse.getString(path)) {
			Assert.assertEquals(jsonMongoDB.getString(path), "0", "validate: " + path);
		} else {
			// Assert.assertEquals(jsonMongoDB.getString(path),
			// jsonResponse.getString(path), "validate: "+path);
			Assert.assertNotNull(jsonMongoDB.getString(path), "validateNotNull-DB: " + path);
			Assert.assertNotNull(jsonResponse.getString(path), "validateNotNull-Resp: " + path);
		}
	}

	public void validateNotNull(JsonPath jsonMongoDB, String path) throws Exception {
		Assert.assertNotNull(jsonMongoDB.getString(path), "validateNotNull: " + path);
	}

	/**
	 * Method used to validate JSON response is not null
	 * 
	 * @param jsonMongoDB
	 * @param jsonResponse
	 * @param path
	 * @throws Exception
	 */
	public void validateNotNull(JsonPath jsonMongoDB, JsonPath jsonResponse, String path) throws Exception {
		Assert.assertNotNull(jsonMongoDB.getString(path), "validateNotNull-DB: " + path);
		Assert.assertNotNull(jsonResponse.getString(path), "validateNotNull-Resp: " + path);
	}

	public void validateError(Response response, String errorCode, String paramV) {
		validateError(response, null, errorCode, paramV);
	}

	public void validateError(Response response, Integer statusCode, String errorCode, String paramV) {
		validateError(response, statusCode, errorCode, paramV, null);
	}

	/**
	 * Method used to validate Http status codes and error codes in JSON
	 * response
	 * 
	 * @param response
	 * @param statusCode
	 * @param errorCode
	 * @param paramV
	 * @param messageV
	 */
	public void validateError(Response response, Integer statusCode, String errorCode, String paramV, String messageV) {
		if (null != statusCode) {
			response.then().statusCode(statusCode);
		}
		ArrayList<Object> errors = response.path("errors");
		if (null != errors && !errors.isEmpty()) {
			System.out.println("checking candidate.aliases (" + errors.size() + ")...");
			boolean found = false;
			for (int i = 0; i < errors.size(); i++) {
				String code = response.path("errors[" + i + "].code");
				String message = response.path("errors[" + i + "].message");
				String param = response.path("errors[" + i + "].param");
				System.out.println("message from response :[" + message + "]:: message from method:[" + messageV + "]");
				System.out.println(
						"error code from response :[" + code + "]:: error code from method:[" + errorCode + "]");
				// if message is not provided then just check if message value
				// exists.
				// Do not need to match content.
				// However, if it is provided then match content
				if (null == messageV) {
					if (code.equals(errorCode) && ((paramV == null && param == null) || param.equals(paramV))
							&& (!StringUtils.isBlank(message))) {
						found = true;
						break;
					}
				} else {
					if (code.equals(errorCode) && ((paramV == null && param == null) || param.equals(paramV))
							&& (message.equals(messageV))) {
						found = true;
						break;
					}
				}
			}
			if (!found) {
				Assert.fail("Expected error not found: [" + errorCode + "|" + paramV + "]");
			}
		}
	}

	/**
	 * Method used to validate a successful Http status code in JSON response
	 * 
	 * @param response
	 */
	public void validateSuccessResponse(Response response) {
		Assert.assertTrue(response.getStatusCode() == 200);
	}

}
