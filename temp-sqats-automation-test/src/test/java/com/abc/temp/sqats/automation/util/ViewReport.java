package com.abc.temp.sqats.automation.util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class provides methods to view the client report content
 * 
 * @author hmalick
 *
 */
public class ViewReport extends BaseUtil {

	private static final Logger log = LoggerFactory.getLogger(ViewReport.class);

	WaitForElementOnPage wait = new WaitForElementOnPage();

	/**
	 * Method is used for viewing the client report content.
	 * 
	 * @param driver
	 *            Always Required
	 * @param Link_Text_to_be_clicked
	 *            Link to be clicked
	 * @param Text_to_find_on_new_window
	 *            What specific text to find on the new window
	 * @param Print_if_found
	 *            What to log if found
	 * @param Print_if_not_found
	 *            What to log if NOT found
	 */
	public void clickSearchIdViewReport(WebDriver driver,
			String Link_Text_to_be_clicked, String Text_to_find_on_new_window,
			String Print_if_found, String Print_if_not_found) {

		String baseWindowHdl = driver.getWindowHandle();

		log.info("Clicking Search ID link");
		// Clicks the search id
		driver.findElement(By.linkText(Link_Text_to_be_clicked)).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		// Goes to report
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		// Wait for Educate url link to be present on bottom of report
		//wait.waitForXpath(driver, Report_Link);
		driver.manage().window().maximize();

		// Find specific text on the report and print messages depending on
		// found or not
		if (driver.getPageSource().contains(Text_to_find_on_new_window)) {
			log.info(Print_if_found);
		} else {
			log.warn(Print_if_not_found);
		}

		// Close pop-up
		driver.close();

		// Switch back to base window
		driver.switchTo().window(baseWindowHdl);
	}
	
	
	public void clickATMCliientReport(WebDriver driver,
			String Link_Text_to_be_clicked, String Text_to_find_on_new_window,
			String Print_if_found, String Print_if_not_found) {

		String baseWindowHdl = driver.getWindowHandle();

		/*driver.findElement(By.xpath(Link_Text_to_be_clicked)).click();*/
		// Clicks the search id
		
		log.info("Waiting for Client Report");
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		// Goes to report
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		// Wait for Educate url link to be present on bottom of report
		//wait.waitForXpath(driver, Report_Link);
		driver.manage().window().maximize();
		wait.waitForcssButton(driver, "input[value='Print']");
		// Find specific text on the report and print messages depending on
		// found or not
		if (driver.getPageSource().contains(Text_to_find_on_new_window)) {
			log.info(Print_if_found);
		} else {
			log.warn(Print_if_not_found);
		}

		// Close pop-up
		driver.close();

		// Switch back to base window
		driver.switchTo().window(baseWindowHdl);
	}

}
