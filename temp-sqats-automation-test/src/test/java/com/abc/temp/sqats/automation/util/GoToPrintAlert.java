package com.abc.temp.sqats.automation.util;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class is a base util class used to print alerts from popups 
 * 
 * @author hmalick
 *
 */
public class GoToPrintAlert extends BaseUtil {

	private static final Logger log = LoggerFactory
			.getLogger(GoToPrintAlert.class);

	private final static String Report_Link = "//*[@id='table11']/tbody/tr/td[2]/span[2]/a";

	WaitForElementOnPage wait = new WaitForElementOnPage();

	public void clickSearchIdViewReport(WebDriver driver,
			String Link_Text_to_be_clicked, String Text_to_find_on_new_window,
			String Print_if_found, String Print_if_not_found) {

		String baseWindowHdl = driver.getWindowHandle();

		log.info("Clicking Search ID link");
		// Clicks the search id
		driver.findElement(By.linkText(Link_Text_to_be_clicked)).click();
		// Goes to report
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		// Wait for Educate url link to be present on bottom of report
		wait.waitForXpath(driver, Report_Link);

		// Find specific text on the report and print messages depending on
		// found or not
		if (driver.getPageSource().contains(Text_to_find_on_new_window)) {
			log.info(Print_if_found);
		} else {
			log.warn(Print_if_not_found);
		}

		// Close pop-up
		driver.close();

		// Switch back to base window
		driver.switchTo().window(baseWindowHdl);
	}

	public void goToPrintAlertAndEscape(WebDriver driver) throws AWTException,
			InterruptedException {

		// How to handle print dialog in Selenium

		log.info("******WHERE IS THE PRINT ALERT*****");
		wait.wait(10);
		Alert printDialog = driver.switchTo().alert();
		printDialog.dismiss();

		// press CANCEL programatically - the print dialog must have focus,
		// obviously
		log.info("******CANCEL PRINT JOB*****");
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_CANCEL);
		r.keyRelease(KeyEvent.VK_CANCEL);

	}

}
