package com.abc.temp.sqats.automation.util;

import static com.jayway.restassured.RestAssured.given;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

/**
 * Class provides methods to execute RESTful API calls
 * 
 * @author hmalick
 *
 */
public class TestUtils {

	private static final Logger log = LoggerFactory.getLogger(TestUtils.class);

	public static final String TEST_RESOURCE_FOLDER = ParameterDataSetup.testFilePath;

	public static final String JSON_PATH_ERRORS = "$.errors";
	public static final String JSON_PATH_ERROR_CODE = "$.errors[0].code";
	public static final String JSON_PATH_ERROR_MESSAGE = "$.errors[0].message";
	public static final String JSON_PATH_ERROR_PARAM = "$.errors[0].param";

	public static void print(Response response) {
		// System.out.println("/**********************************************************************/");
		System.out.println("------------------------------------------------------------------------");
		System.out.println(" RESPONSE: ");
		System.out.println(" StatusCode: " + response.getStatusCode());
		System.out.println(" ContentType: " + response.getContentType());
		System.out.println(" Response: " + response.asString());
		// System.out.println("/**********************************************************************/");
	}

	public static Response run(String url, HttpMethod httpMethod, MediaType mediaType, String requestFilename)
			throws Exception {
		return run(url, httpMethod, mediaType, requestFilename, null);
	}

	public static Response run(String url, HttpMethod httpMethod, MediaType mediaType, String requestFilename,
			Map<String, Object> header) throws Exception {
		return run(url, httpMethod, mediaType, requestFilename, header, null, null);
	}

	public static Response run(String url, HttpMethod httpMethod, MediaType mediaType, String requestFilename,
			Map<String, Object> header, String jsonRequestString) throws Exception {
		return run(url, httpMethod, mediaType, requestFilename, header, jsonRequestString, null);
	}

	public static Response run(String url, HttpMethod httpMethod, MediaType mediaType, String requestFilename,
			Map<String, Object> header, String jsonRequestString, Map<String, String> duplicateCheck) throws Exception {
		return run(url, httpMethod, mediaType, requestFilename, header, jsonRequestString, duplicateCheck, null);
	}

	/**
	 * Method is a common method used to execute RESTful API calls. Provide the
	 * following parameters
	 * 
	 * @param url
	 * @param httpMethod
	 * @param mediaType
	 * @param requestFilename
	 * @param header
	 * @param jsonRequestString
	 * @param duplicateCheck
	 * @param basicAuth
	 * @return
	 * @throws Exception
	 */
	public static Response run(String url, HttpMethod httpMethod, MediaType mediaType, String requestFilename,
			Map<String, Object> header, String jsonRequestString, Map<String, String> duplicateCheck,
			Map<String, String> basicAuth) throws Exception {

		RequestSpecification request = given().relaxedHTTPSValidation("TLSv1.2").log().all()
				.contentType(mediaType.toString());

		if (mediaType.equals(MediaType.APPLICATION_JSON) || mediaType.equals(MediaType.TEXT_PLAIN)) {
			String jsonRequest = null;
			if (null != jsonRequestString) {
				jsonRequest = jsonRequestString;
			} else {
				if (null != requestFilename) {
					jsonRequest = TestUtils.getFileJsonContentFromTestResource(requestFilename);
				}
			}
			if (null != jsonRequest) {
				request.body(jsonRequest);
			}
		} else if (mediaType.equals(MediaType.APPLICATION_FORM_URLENCODED)
				|| mediaType.equals(MediaType.MULTIPART_FORM_DATA)) {
			if (null != requestFilename) {
				Properties prop = getParamsFromTestResource(requestFilename);
				Enumeration<?> e = prop.propertyNames();
				while (e.hasMoreElements()) {
					String key = (String) e.nextElement();
					if (null != duplicateCheck) {
						for (Map.Entry<String, String> element : duplicateCheck.entrySet()) {
							System.out.print("DuplicateCheck: Key is: " + element.getKey() + " & Value is: "
									+ element.getValue() + "\n");
							if (key.equals(element.getKey())) {
								prop.setProperty(key, element.getValue());
							}
						}
					}
					String value = prop.getProperty(key);
					System.out.println("TestUtils.setParams:  Key : " + key + ", Value : " + value);
					String[] val = { value };
					request.formParam(key, val);
				}
			}
		} else if (mediaType.equals(MediaType.APPLICATION_OCTET_STREAM)) {
			if (null != requestFilename) {
				request.header("Content-Type", MediaType.APPLICATION_FORM_URLENCODED.toString());
				// url = url+"?";
				Properties prop = getParamsFromTestResource(requestFilename);
				Enumeration<?> e = prop.propertyNames();
				while (e.hasMoreElements()) {
					String key = (String) e.nextElement();
					if (null != duplicateCheck) {
						for (Map.Entry<String, String> element : duplicateCheck.entrySet()) {
							System.out.print("DuplicateCheck: Key is: " + element.getKey() + " & Value is: "
									+ element.getValue() + "\n");
							if (key.equals(element.getKey())) {
								prop.setProperty(key, element.getValue());
							}
						}
					}
					String value = prop.getProperty(key);
					System.out.println("TestUtils.setParams:  Key : " + key + ", Value : " + value);
					String[] val = { value };
					// url +="&"+key + "=" + value;
					request.queryParam(key, value);
				}
			}
		} else {
			throw new Exception("Error in Test: Unsupported media type: " + mediaType);
		}

		if (null != header) {
			for (Map.Entry<String, Object> element : header.entrySet()) {
				System.out.print("Header: Key is: " + element.getKey() + " & Value is: " + element.getValue() + "\n");
				request.header(element.getKey(), element.getValue());
			}
		}

		if (null != basicAuth) {
			for (Map.Entry<String, String> element : basicAuth.entrySet()) {
				System.out.print("BasicAuth: username: " + element.getKey() + "\n");
				System.out.print("BasicAuth: password: " + element.getValue() + "\n");
				request.auth().preemptive().basic(element.getKey(), element.getValue());
			}
		}

		Response response = null;
		if (httpMethod.equals(HttpMethod.POST)) {
			response = request.when().post(url);
		} else if (httpMethod.equals(HttpMethod.PUT)) {
			response = request.when().put(url);
		} else if (httpMethod.equals(HttpMethod.GET)) {
			response = request.when().get(url);
		} else if (httpMethod.equals(HttpMethod.PATCH)) {
			response = request.when().patch(url);
		} else if (httpMethod.equals(HttpMethod.DELETE)) {
			response = request.when().delete(url);
		} else {
			throw new Exception("Error in Test: Unsupported http method: " + httpMethod);
		}

		print(response);

		return response;

	}

	/** ================= Get content of file ================= */
	public static String getFileJsonContentFromTestResource(String filename) throws Exception {
		return new String(Files.readAllBytes(Paths.get(TEST_RESOURCE_FOLDER + "json/" + filename)));
	}

	public static String getFileContentFromTestResource(String filename) throws Exception {
		return new String(Files.readAllBytes(Paths.get(TEST_RESOURCE_FOLDER + filename)));
	}

	public static String getFileContent(String filenameWithPath) throws Exception {
		return new String(Files.readAllBytes(Paths.get(filenameWithPath)));
	}

	/** ================= Get content of file as Properties ================= */
	public static Properties getParamsFromTestResource(String filename) throws Exception {
		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream(TEST_RESOURCE_FOLDER + "params/" + filename);
		prop.load(input);
		return prop;
	}

	public static Properties getPropertiesFromTestResource(String filename) throws Exception {
		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream(TEST_RESOURCE_FOLDER + filename);
		prop.load(input);
		return prop;
	}

	public static Properties getProperties(String filenameWithPath) throws Exception {
		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream(filenameWithPath);
		prop.load(input);
		return prop;
	}

	/** ================= Construct url with querystring ================= */
	public static String getUrl(String url, String filenameWithPath) throws Exception {
		String newUrl = url + "?";
		Properties prop = getProperties(filenameWithPath);
		Enumeration<?> e = prop.propertyNames();
		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			String value = prop.getProperty(key);
			System.out.println("Key : " + key + ", Value : " + value);
			newUrl += "&" + key + "=" + value;
		}
		return newUrl;
	}

	public static String getUrlFromTestResource(String url, String filename) throws Exception {
		String newUrl = url + "?";
		Properties prop = getPropertiesFromTestResource(filename);
		Enumeration<?> e = prop.propertyNames();
		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			String value = prop.getProperty(key);
			System.out.println("Key : " + key + ", Value : " + value);
			newUrl += "&" + key + "=" + value;
		}
		return newUrl;
	}

}
