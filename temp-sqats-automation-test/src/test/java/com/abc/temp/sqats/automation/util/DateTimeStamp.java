package com.abc.temp.sqats.automation.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Class is a base util class used to generate Date Time stamps
 * 
 * @author hmalick
 *
 */
public class DateTimeStamp {

	/**
	 * @return DateTimeStamp
	 */
	public String GetTimeStampValue() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String value = dateFormat.format(cal.getTime()).toString();
		value = value.replace("/", "");
		value = value.replace(":", "");
		value = value.replace(" ", "_");
		return value;
	}

}
