package com.abc.temp.sqats.automation.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

/**
 * Class provides methods to perform functionalities on the Amazon RDS database
 * 
 * @author hmalick
 *
 */
public class AmazonRDSDBUtil {

	private static final Logger log = LoggerFactory.getLogger(AmazonRDSDBUtil.class);

	/**
	 * Method is used to retrieve a single record from the Amazon RDS database.
	 * Provide the Amazon RDS query, table name, database connection URL,
	 * connection username and connection password
	 * 
	 * @param query
	 * @param table
	 * @param dbconnURL
	 * @param dbconnUsername
	 * @param dbconnPassword
	 * @return
	 */
	public Map<String, String> dbConnectRDSFetchSingleRecordString(String query, String table, String dbconnURL,
			String dbconnUsername, String dbconnPassword) {
		List<Map<String, String>> records = dbConnectRDSFetchRecordsString(query, table, dbconnURL, dbconnUsername,
				dbconnPassword);
		if (null != records && !records.isEmpty()) {
			return records.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Method is used to retrieve multiple records from the Amazon RDS database.
	 * Provide the Amazon RDS query, table name, database connection URL,
	 * connection username and connection password
	 * 
	 * @param query
	 * @param table
	 * @param dbconnURL
	 * @param dbconnUsername
	 * @param dbconnPassword
	 * @return
	 */
	public List<Map<String, String>> dbConnectRDSFetchRecordsString(String query, String table, String dbconnURL,
			String dbconnUsername, String dbconnPassword) {

		List<Map<String, String>> records = null;
		try {

			log.info("Connection URI: " + dbconnURL);
			log.info("Connection username/password: " + dbconnUsername + "/" + dbconnPassword);
			log.info("Connected to driver " + "com.mysql.cj.jdbc.Driver");
			log.info("query: " + query);

			Map<String, String> columns = dbConnectRDSFetchColumnsOfTable(table, dbconnURL, dbconnUsername,
					dbconnPassword);

			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(dbconnURL, dbconnUsername, dbconnPassword);

			Statement statement = conn.createStatement();

			String queryString = query;
			log.info("Query String: " + queryString);
			ResultSet rs = statement.executeQuery(queryString);

			records = getAsMapString(rs, columns);

			log.info("dbConnectRDSFetchRecords from database.");
			conn.close();
		} catch (Exception e) {
			log.error("Unable to run dbConnectRDSFetchRecords.");
			log.error("Fail ", e);
			Assert.fail("Unable to run dbConnectRDSFetchRecords.", e);
		}
		return records;
	}

	/**
	 * Method is used to retrieve column names of a table from the Amazon RDS
	 * database. Provide the Amazon RDS query, table name, database connection
	 * URL, connection username and connection password
	 * 
	 * @param query
	 * @param table
	 * @param dbconnURL
	 * @param dbconnUsername
	 * @param dbconnPassword
	 * @return
	 */
	public Map<String, String> dbConnectRDSFetchColumnsOfTable(String tableName, String dbconnURL,
			String dbconnUsername, String dbconnPassword) {

		Map<String, String> map = new HashMap<String, String>();
		try {

			log.info("Connection URI: " + dbconnURL);
			log.info("Connection username/password: " + dbconnUsername + "/" + dbconnPassword);
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");
			log.info("tableName: " + tableName);

			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(dbconnURL, dbconnUsername, dbconnPassword);

			Statement statement = conn.createStatement();

			String queryString = " SELECT c.COLUMN_NAME, c.DATA_TYPE " + " FROM INFORMATION_SCHEMA.COLUMNS c "
					+ " INNER JOIN INFORMATION_SCHEMA.TABLES t " + " ON c.TABLE_CATALOG = t.TABLE_CATALOG "
					+ " AND c.TABLE_NAME = t.TABLE_NAME " + " AND c.TABLE_SCHEMA = t.TABLE_SCHEMA "
					+ " and c.TABLE_NAME='" + tableName + "'";
			log.info("Query String: " + queryString);
			ResultSet rs = statement.executeQuery(queryString);

			while (rs.next()) {
				// log.info("Database Value: "+rs.toString());
				map.put(rs.getString(1), rs.getString(2));
			}
			log.info("dbConnectRDSFetchColumnsOfTable details from database.");
			conn.close();
		} catch (Exception e) {
			log.error("Unable to get dbConnectRDSFetchColumnsOfTable details.");
			log.error("Fail ", e);
			Assert.fail("Unable to get dbConnectRDSFetchColumnsOfTable details.", e);
		}
		return map;
	}

	/**
	 * Method to get record as map string. Provide the Amazon RDS result set and
	 * columns
	 * 
	 * @param rs
	 * @param columns
	 * @return
	 */
	public List<Map<String, String>> getAsMapString(ResultSet rs, Map<String, String> columns) {
		List<Map<String, String>> records = new ArrayList<Map<String, String>>();
		try {
			while (rs.next()) {
				Map<String, String> aRecord = new HashMap<String, String>();
				for (Map.Entry<String, String> element : columns.entrySet()) {
					String columnName = element.getKey();
					String columnValue = rs.getString(columnName);
					aRecord.put(columnName, columnValue);
				}
				records.add(aRecord);
			}

		} catch (Exception e) {
			Assert.fail("Unable to run ResultSet.", e);
		}
		return records;
	}

}
