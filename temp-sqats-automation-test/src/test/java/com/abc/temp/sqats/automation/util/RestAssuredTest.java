package com.abc.temp.sqats.automation.util;

import org.junit.Before;
import org.junit.Test;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.expect;
import static org.hamcrest.Matchers.equalTo;

/**
 * Class provides methods to test the RestAssured functionality
 * 
 * @author hmalick
 *
 */
public class RestAssuredTest {

	@Before
	public void setup() {
		RestAssured.baseURI = "http://10.34.180.65";
		RestAssured.port = 9205;
	}

	// @Test
	public void simpleTest() {

		Response response = given().when().get("/v1/creds/alive");

		System.out.println(response.asString());

		response.then().statusCode(200).body("alive", equalTo(false));

		// ResponseBody responseBody = given().get("/v1/creds/alive").body();

		// System.out.println(responseBody.asString());

		// http://localhost:9220/v1/account/56671cb21f6a6f54858fba64
		// http://10.34.180.65:9205/v1/creds/

	}

	@Test
	public void complexTest() {

		Response response = given().queryParam("accountId", "some-account-id").queryParam("type", "live").when()
				.get("/v1/creds");

		System.out.println(response.asString());

		response.then().statusCode(200).body("id[0]", equalTo("56652bb11f6af720c972a8b1")).and()
				.body("clientCode[0]", equalTo("90dd7fc8-1e00-49d6-8e96-9a5c92df75c4")).and()
				.body("tags.accountId[0]", equalTo("some-account-id"));

		// accountId=some-account-id&type=live&status=enabled
		// ResponseBody responseBody = given().get("/v1/creds/alive").body();

		// System.out.println(responseBody.asString());

		// http://localhost:9220/v1/account/56671cb21f6a6f54858fba64
		// http://10.34.180.65:9205/v1/creds/

	}

}
