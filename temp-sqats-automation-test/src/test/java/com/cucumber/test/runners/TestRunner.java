package com.cucumber.test.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

/**
 * Class utilizes the feature file to run Cucumber Test/s using CucumberOptions
 * and generate Cucmber test results in html and json formats
 * 
 * @author hmalick
 *
 */
@CucumberOptions(features = "src/test/resources/features/Test.feature", glue = "com.cucumber.test.glue", format = {
		"pretty", "html:target/cucumber/cucumber1/html", "json:target/cucumber/cucumber1/json/cucumber.json" })
public class TestRunner extends AbstractTestNGCucumberTests {
}