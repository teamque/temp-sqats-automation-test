Feature: Google Search
  As a user, I need to perform a Google Search using search data and return the search results
    
   Scenario: Perform a Google Search
    Given I navigate to the "google search" page with the url
	When I perform the "google search" using the search data
	Then The "google search" should be successful
	And The search result should be displayed