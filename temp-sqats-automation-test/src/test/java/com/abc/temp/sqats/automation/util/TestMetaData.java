package com.abc.temp.sqats.automation.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * TestMetaData is a custom annotation used in every test method to provide the
 * testlink test case id for execution
 * 
 * @author hmalick
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) // can use in method only.
public @interface TestMetaData {
	String testcase();

	String title() default "";
}