package com.abc.temp.sqats.automation.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.jmeter.protocol.http.util.Base64Encoder;
import org.apache.soap.providers.com.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class is a base util class the provides http methods for RESTful API Test/s
 * 
 * @author hmalick
 *
 */
public class HttpUtil {
	private static final Logger log = LoggerFactory
			.getLogger(HttpUtil.class);
	
	static RequestConfig defaultRequestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.BEST_MATCH).setExpectContinueEnabled(true)
		.setStaleConnectionCheckEnabled(true).setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM, AuthSchemes.DIGEST))
		.setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC)).build();


	public static HttpGet createGetRequest(String url, String user, String password) {
       	HttpGet getRequest = new HttpGet(url); 
       	       	
       	String encodedCredentials= Base64Encoder.encode (user+":"+password);
       	getRequest.setHeader("Authorization", "Basic " + encodedCredentials);
       	
       	RequestConfig requestConfig = RequestConfig.copy(defaultRequestConfig).setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
       	getRequest.setConfig(requestConfig);
       	return getRequest;
	}
	
	public static HttpPost createPostRequest(String url, String user, String password) {
       	HttpPost getRequest = new HttpPost(url); 
       	
       	String encodedCredentials= Base64Encoder.encode (user+":"+password);
       	getRequest.setHeader("Authorization", "Basic " + encodedCredentials);
       	return getRequest;
	}
	
	public static HttpPost createPostRequest(String url, String user, String password, String fileName) throws IOException {
       	HttpPost request = new HttpPost(url); 
       	
       	String encodedCredentials= Base64Encoder.encode (user+":"+password);
       	request.setHeader("Authorization", "Basic " + encodedCredentials);
       	request.setEntity(readJsonRequestFromFile(fileName));
       	
       	return request;
	}
	
	public static HttpPut createPostRequestString(String url, String user, String password, String jsonString) throws IOException {
		HttpPut request = new HttpPut(url); 
       	
       	String encodedCredentials= Base64Encoder.encode (user+":"+password);
       	request.setHeader("Authorization", "Basic " + encodedCredentials);
       	StringEntity input = new StringEntity(jsonString);
       	input.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
       	request.setEntity(input);
       	       	
       	return request;
	}
	
	public static String getResponseContent(HttpEntity httpEntity) throws Exception{
        
        String inputStreamString = new Scanner(httpEntity.getContent(), "UTF-8").useDelimiter("\\A").next();
        return inputStreamString;
	}	
	
	public static StringEntity readJsonRequestFromFile(String fileName) throws IOException {
    	InputStream requestInputStream = new FileInputStream( new File(fileName) );
    	String json = IOUtils.toString(requestInputStream);
    	StringEntity input = new StringEntity(json);
    	
    	return input;
	}
	
	public static HttpPost createPostRequestNoAuthorization(String url, String fileName) throws IOException {
        log.info("Method: POST");
		HttpPost request = new HttpPost(url); 
       	
       	request.setEntity(readJsonRequestFromFile(fileName));
       	
       	return request;
	}
	
	public static HttpGet createGetRequestNoAuthorization(String url) {
		log.info("Method: GET");
       	HttpGet getRequest = new HttpGet(url); 
       	
       	RequestConfig requestConfig = RequestConfig.copy(defaultRequestConfig).setSocketTimeout(5000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();
       	getRequest.setConfig(requestConfig);
       	return getRequest;
	}
	
	public static HttpPut createPutRequestNoAuthorization(String url, String fileName) throws IOException {
		log.info("Method: PUT");
       	HttpPut putRequest = new HttpPut(url); 
       	
       	putRequest.setEntity(readJsonRequestFromFile(fileName));
       	return putRequest;
	}

}
