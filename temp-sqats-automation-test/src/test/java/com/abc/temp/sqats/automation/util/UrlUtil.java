package com.abc.temp.sqats.automation.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Class provides methods to generate a url
 * 
 * @author hmalick
 *
 */
public class UrlUtil {

	private Map<String, String> data = new HashMap<String, String>();

	public Map<String, String> getData() {
		return data;
	}

	public UrlUtil setData(Map<String, String> data) {
		this.data = data;
		return this;
	}

	public UrlUtil resetData() {
		data = new HashMap<String, String>();
		return this;
	}

	public UrlUtil addData(String key, String value) {
		data.put(key, value);
		return this;
	}

	public String getUrl(String url) throws Exception {
		String newUrl = url;
		for (Map.Entry<String, String> element : data.entrySet()) {
			newUrl = url.replaceAll("\\{" + element.getKey() + "\\}", element.getValue());
		}
		return newUrl;
	}

	/**
	 * Method is used to generate a Url. Provide the parameters as follows
	 * baseUrl: http://localhost:9205 
	 * path: /v1/creds?accountId={accountId}
	 * return output: http://localhost:9205/v1/creds?accountId=123456
	 * 
	 * @param baseUrl
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public String getUrl(String baseUrl, String path) throws Exception {
		String newUrl = baseUrl + path;
		for (Map.Entry<String, String> element : data.entrySet()) {
			newUrl = newUrl.replaceAll("\\{" + element.getKey() + "\\}", element.getValue());
		}
		return newUrl;
	}

	/**
	 * Method is used to generate a Url. Provide the parameters as follows
	 * http://localhost:9205/v1/creds?accountId={accountId} 
	 * data key: "accountId"
	 * data value: "123456" 
	 * return output: http://localhost:9205/v1/creds?accountId=123456
	 * 
	 * @param url
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public String getUrl(String url, Map<String, String> data) throws Exception {
		String newUrl = url;
		for (Map.Entry<String, String> element : data.entrySet()) {
			newUrl = url.replaceAll("\\{" + element.getKey() + "\\}", element.getValue());
		}
		return newUrl;
	}

	public static void main(String[] args) {

		try {
			UrlUtil urlUtil = new UrlUtil();

			/*
			 * Map<String, String> data = new HashMap<String, String>();
			 * data.put("accountId", "123456"); System.out.println(
			 * urlUtil.getUrl(
			 * "http://localhost:9205/v1/creds?accountId={accountId}", data) );
			 * System.out.println(
			 * urlUtil.getUrl("http://localhost:9205/v1/creds", data) );
			 */

			// urlUtil.resetData().addData("accountId",
			// "123456").getUrl("http://localhost:9205/v1/creds?accountId={accountId}");

			System.out.println(urlUtil.resetData().addData("accountId", "123456").addData("env", "DEV")
					.getUrl("http://localhost:9205/v1/creds?accountId={accountId}"));

			System.out.println(urlUtil.resetData().addData("accountId", "123456").addData("env", "DEV")
					.getUrl("http://localhost:9205", "/v1/creds?accountId={accountId}"));

			System.out.println(urlUtil.resetData().addData("accountId", "123456").addData("env", "DEV")
					.getUrl("http://localhost:9205", "/v1/creds/{accountId}"));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
