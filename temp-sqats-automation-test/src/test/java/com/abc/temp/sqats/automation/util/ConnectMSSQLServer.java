package com.abc.temp.sqats.automation.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

/**
 * Class is a base util class used for connect and execute queries on MSSQL
 * 
 * @author hmalick
 * 
 */
public class ConnectMSSQLServer {

	private static final Logger log = LoggerFactory.getLogger(ConnectMSSQLServer.class);

	private String[] dbConnectFetchSingleRecord(String query, int columns, String dbconnURL, String dbconnUsername,
			String dbconnPassword) {

		String[] contents = new String[columns];
		try {

			log.info("Connection URI: " + dbconnURL);
			log.info("Connection username/password: " + dbconnUsername + "/" + dbconnPassword);
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");
			log.info("query: " + query);

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn = DriverManager.getConnection(dbconnURL, dbconnUsername, dbconnPassword);

			Statement statement = conn.createStatement();

			String queryString = "USE " + ParameterDataSetup.dbconnDatabase + " " + query;
			log.info("Query String: " + queryString);
			ResultSet rs = statement.executeQuery(queryString);

			while (rs.next()) {
				if (rs.getString(1) != null) {
					for (int i = 0; i < contents.length; i++) {
						contents[i] = rs.getString(i + 1);
						// log.info("Database Value: "+contents[i]);
					}
				} else {
					log.error("Company not found");
					for (int i = 0; i < contents.length; i++) {
						contents[i] = "";
					}
				}
			}
			log.info("dbConnectFetchSingleRecord details from database.");
			conn.close();
		} catch (Exception e) {
			log.error("Unable to get dbConnectFetchSingleRecord details.");
			log.error("Fail ", e);
			Assert.fail("Unable to get dbConnectFetchSingleRecord details.", e);
		}
		return contents;
	}

	public Map<String, Object> dbConnectFetchSingleRecord(String query, String table, String dbconnURL,
			String dbconnUsername, String dbconnPassword) {
		List<Map<String, Object>> records = dbConnectFetchRecords(query, table, dbconnURL, dbconnUsername,
				dbconnPassword);
		if (null != records && !records.isEmpty()) {
			return records.get(0);
		} else {
			return null;
		}
	}

	public List<Map<String, Object>> dbConnectFetchRecords(String query, String table, String dbconnURL,
			String dbconnUsername, String dbconnPassword) {

		List<Map<String, Object>> records = null;
		try {

			log.info("Connection URI: " + dbconnURL);
			log.info("Connection username/password: " + dbconnUsername + "/" + dbconnPassword);
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");
			log.info("query: " + query);

			Map<String, String> columns = dbConnectFetchColumnsOfTable(table, dbconnURL, dbconnUsername,
					dbconnPassword);

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn = DriverManager.getConnection(dbconnURL, dbconnUsername, dbconnPassword);

			Statement statement = conn.createStatement();

			String queryString = "USE " + ParameterDataSetup.dbconnDatabase + " " + query;
			log.info("Query String: " + queryString);
			ResultSet rs = statement.executeQuery(queryString);

			records = getAsMap(rs, columns);

			log.info("dbConnectFetchRecords from database.");
			conn.close();
		} catch (Exception e) {
			log.error("Unable to run dbConnectFetchRecords.");
			log.error("Fail ", e);
			Assert.fail("Unable to run dbConnectFetchRecords.", e);
		}
		return records;
	}

	public Map<String, List<Map<String, Object>>> fetchSearchRecords(String query, String dbconnURL,
			String dbconnUsername, String dbconnPassword) {
		Map<String, List<Map<String, Object>>> records = new HashMap<String, List<Map<String, Object>>>();
		List<Map<String, Object>> raw = dbConnectFetchRecords(query, "SEARCH", dbconnURL, dbconnUsername,
				dbconnPassword);
		if (null != raw) {
			for (Map<String, Object> map : raw) {
				String key = (String) map.get("search_type_code");
				List<Map<String, Object>> list = records.get(key);
				if (null != list) {
					list.add(map);
				} else {
					list = new ArrayList<Map<String, Object>>();
					list.add(map);
				}
				records.put(key, list);
			}
		}
		return records;
	}

	public Map<String, String> dbConnectFetchSingleRecordString(String query, String table, String dbconnURL,
			String dbconnUsername, String dbconnPassword) {
		List<Map<String, String>> records = dbConnectFetchRecordsString(query, table, dbconnURL, dbconnUsername,
				dbconnPassword);
		if (null != records && !records.isEmpty()) {
			return records.get(0);
		} else {
			return null;
		}
	}

	public List<Map<String, String>> dbConnectFetchRecordsString(String query, String table, String dbconnURL,
			String dbconnUsername, String dbconnPassword) {

		List<Map<String, String>> records = null;
		try {

			log.info("Connection URI: " + dbconnURL);
			log.info("Connection username/password: " + dbconnUsername + "/" + dbconnPassword);
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");
			log.info("query: " + query);

			Map<String, String> columns = dbConnectFetchColumnsOfTable(table, dbconnURL, dbconnUsername,
					dbconnPassword);

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn = DriverManager.getConnection(dbconnURL, dbconnUsername, dbconnPassword);

			Statement statement = conn.createStatement();

			String queryString = "USE " + ParameterDataSetup.dbconnDatabase + " " + query;
			log.info("Query String: " + queryString);
			ResultSet rs = statement.executeQuery(queryString);

			records = getAsMapString(rs, columns);

			log.info("dbConnectFetchRecords from database.");
			conn.close();
		} catch (Exception e) {
			log.error("Unable to run dbConnectFetchRecords.");
			log.error("Fail ", e);
			Assert.fail("Unable to run dbConnectFetchRecords.", e);
		}
		return records;
	}

	public List<Map<String, String>> getAsMapString(ResultSet rs, Map<String, String> columns) {
		List<Map<String, String>> records = new ArrayList<Map<String, String>>();
		try {
			while (rs.next()) {
				Map<String, String> aRecord = new HashMap<String, String>();
				for (Map.Entry<String, String> element : columns.entrySet()) {
					String columnName = element.getKey();
					String columnValue = rs.getString(columnName);
					aRecord.put(columnName, columnValue);
				}
				records.add(aRecord);
			}

		} catch (Exception e) {
			Assert.fail("Unable to run ResultSet.", e);
		}
		return records;
	}

	/*
	 * 
	 * 
	 * SELECT --c.TABLE_SCHEMA, --c.TABLE_NAME, c.COLUMN_NAME, c.DATA_TYPE,
	 * c.CHARACTER_MAXIMUM_LENGTH, is_nullable --, c.* FROM
	 * INFORMATION_SCHEMA.COLUMNS c INNER JOIN INFORMATION_SCHEMA.TABLES t ON
	 * c.TABLE_CATALOG = t.TABLE_CATALOG AND c.TABLE_NAME = t.TABLE_NAME AND
	 * c.TABLE_SCHEMA = t.TABLE_SCHEMA and c.TABLE_NAME='SEARCH'
	 * 
	 */

	public Map<String, String> dbConnectFetchColumnsOfTable(String tableName, String dbconnURL, String dbconnUsername,
			String dbconnPassword) {

		Map<String, String> map = new HashMap<String, String>();
		try {

			log.info("Connection URI: " + dbconnURL);
			log.info("Connection username/password: " + dbconnUsername + "/" + dbconnPassword);
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");
			log.info("tableName: " + tableName);

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn = DriverManager.getConnection(dbconnURL, dbconnUsername, dbconnPassword);

			Statement statement = conn.createStatement();

			String queryString = "USE " + ParameterDataSetup.dbconnDatabase + " "
					+ " SELECT c.COLUMN_NAME, c.DATA_TYPE " + " FROM INFORMATION_SCHEMA.COLUMNS c "
					+ " INNER JOIN INFORMATION_SCHEMA.TABLES t " + " ON c.TABLE_CATALOG = t.TABLE_CATALOG "
					+ " AND c.TABLE_NAME = t.TABLE_NAME " + " AND c.TABLE_SCHEMA = t.TABLE_SCHEMA "
					+ " and c.TABLE_NAME='" + tableName + "'";
			log.info("Query String: " + queryString);
			ResultSet rs = statement.executeQuery(queryString);

			while (rs.next()) {
				// log.info("Database Value: "+rs.toString());
				map.put(rs.getString(1), rs.getString(2));
			}
			log.info("dbConnectFetchColumnsOfTable details from database.");
			conn.close();
		} catch (Exception e) {
			log.error("Unable to get dbConnectFetchColumnsOfTable details.");
			log.error("Fail ", e);
			Assert.fail("Unable to get dbConnectFetchColumnsOfTable details.", e);
		}
		return map;
	}

	public List<Map<String, Object>> getAsMap(ResultSet rs, Map<String, String> columns) {
		List<Map<String, Object>> records = new ArrayList<Map<String, Object>>();
		try {
			while (rs.next()) {
				Map<String, Object> aRecord = new HashMap<String, Object>();
				for (Map.Entry<String, String> element : columns.entrySet()) {
					String columnName = element.getKey();
					Object columnValue = rs.getObject(columnName);
					aRecord.put(columnName, columnValue);
				}
				records.add(aRecord);
			}

		} catch (Exception e) {
			log.error("getAsMap", e);
			Assert.fail("Unable to run ResultSet.", e);
		}
		return records;
	}

	public void dbConnectSearchUpdate(String updateStmt, String dbconnURL, String dbconnUsername,
			String dbconnPassword) {

		try {

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn = DriverManager.getConnection(dbconnURL, dbconnUsername, dbconnPassword);
			/* System.out.println("connected"); */
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");
			log.info("updateStmt: " + updateStmt);

			String queryString[] = new String[1];

			queryString[0] = "USE " + ParameterDataSetup.dbconnDatabase + " " + updateStmt;

			for (int i = 0; i < 1; i++) {

				String queryScenario = "";
				String queryStringConcat = queryString[i];

				if (i == 0) {
					queryScenario = "update";
				}

				PreparedStatement statement = conn.prepareStatement(queryStringConcat,
						PreparedStatement.RETURN_GENERATED_KEYS);
				statement.executeUpdate();
				log.info("Running query for " + queryScenario + " - " + queryStringConcat);
				ResultSet rs = statement.getGeneratedKeys();

				while (rs.next()) {
					if (rs.getString(1) != null) {
						log.info(queryScenario + " Query executed for Insert: SQL GeneratedKey = " + rs.getLong(1));
					} else {
						log.info(queryScenario + " Query executed for Update: SQL GeneratedKey = " + rs.getLong(1));
					}
				}

			}

			log.info("Updated Search Id details in database.");
			conn.close();
		} catch (Exception e) {
			Assert.fail("Fail", e);
		}
	}

	public List<Map<String, Object>> dbConnect_absp_getStatusChangeRecord(String system, Integer num, String dbconnURL,
			String dbconnUsername, String dbconnPassword) {

		List<Map<String, Object>> records = null;
		try {

			log.info("Connection URI: " + dbconnURL);
			log.info("Connection username/password: " + dbconnUsername + "/" + dbconnPassword);
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");
			log.info("num: " + num);

			Map<String, String> columns = dbConnectFetchColumnsOfTable("SEARCH_STATUS_CHANGE", dbconnURL,
					dbconnUsername, dbconnPassword);

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn = DriverManager.getConnection(dbconnURL, dbconnUsername, dbconnPassword);

			/*
			 * Statement statement = conn.createStatement();
			 * 
			 * String queryString =
			 * "SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME LIKE '%absp_getStatusChangeRecord%'"
			 * ; log.info("Query String: "+queryString); ResultSet rs =
			 * statement.executeQuery(queryString); while (rs.next()) {
			 * log.info("Database Value: "+rs.toString());
			 * log.info(rs.getString(1)+rs.getString(2)+rs.getString(3)+rs.
			 * getString(4)); }
			 */

			CallableStatement cs = conn.prepareCall("{call dbo.absp_getStatusChangeRecord(?, ?)}");
			cs.setString(1, system);
			cs.setInt(2, num);

			ResultSet rs = cs.executeQuery();

			records = getAsMap(rs, columns);

			log.info("dbConnectFetchRecords from database.");
			conn.close();
		} catch (Exception e) {
			log.error("Unable to run dbConnectFetchRecords.");
			log.error("Fail ", e);
			Assert.fail("Unable to run dbConnectFetchRecords.", e);
		}
		return records;
	}

}
