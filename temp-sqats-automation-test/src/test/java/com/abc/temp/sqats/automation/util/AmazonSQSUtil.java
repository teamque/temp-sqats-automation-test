package com.abc.temp.sqats.automation.util;

import java.util.List;
import java.util.ListIterator;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.ListQueuesResult;
import com.amazonaws.services.sqs.model.Message;

/**
 * Class provides methods to perform functionalities on the Amazon SQS queue
 * 
 * @author hmalick
 *
 */
public class AmazonSQSUtil {

	private static BasicAWSCredentials credentials;
	private static AmazonSQS sqs;
	private static String simpleQueue = "dev-order-integration-queue-lp";
	private static String accessKey = "AKIAJJX3UXUDRDPZZRHQ";
	private static String secretKey = "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";
	private static String awsEndPoint = "https://sqs.us-west-2.amazonaws.com";

	public void getAmazonSQSMessage() {
		getAmazonSQSMessage(simpleQueue, accessKey, secretKey, awsEndPoint);
	}

	public void getAmazonSQSMessage(String simpleQueue) {
		getAmazonSQSMessage(simpleQueue, accessKey, secretKey, awsEndPoint);
	}

	/**
	 * Method is used to retrieve a message from the Amazon SQS queue. Provide
	 * the Amazon SQS simple queue name, access key, secret key and aws end
	 * point
	 * 
	 * @param simpleQueue
	 * @param accessKey
	 * @param secretKey
	 * @param awsEndPoint
	 */
	@SuppressWarnings("deprecation")
	public void getAmazonSQSMessage(String simpleQueue, String accessKey, String secretKey, String awsEndPoint) {

		credentials = new BasicAWSCredentials(accessKey, secretKey);
		System.out.println("credentials: " + credentials.toString());

		sqs = new AmazonSQSClient(credentials);
		System.out.println("sqs: " + sqs);

		sqs.setEndpoint(awsEndPoint);

		listQueues();
		String queueUrl = getQueueUrl(simpleQueue);

		// receive messages from the queue
		List<Message> messages = sqs.receiveMessage(queueUrl).getMessages();
		System.out.println("sqs.receiveMessage: " + messages.toString());

		/*
		 * for (ListIterator<Message> i = messages.listIterator(); i.hasNext();)
		 * {
		 */
		for (int i = 0; i < messages.size(); i++) {
			System.out.println("getMessageSize: " + messages.size());
			System.out.println("getMessageId: " + messages.get(i));
		}
	}

	/**
	 * Method returns the queueurl for the Amazon SQS queue if the queueName is
	 * provided
	 * 
	 * @param queueName
	 * @return
	 */
	public static String getQueueUrl(String queueName) {

		GetQueueUrlRequest getQueueUrlRequest = new GetQueueUrlRequest(queueName);
		System.out.println("sqs.getQueueUrl: " + sqs.getQueueUrl(getQueueUrlRequest).getQueueUrl());
		return sqs.getQueueUrl(getQueueUrlRequest).getQueueUrl();

	}

	/**
	 * Method lists all queues
	 * 
	 * @return
	 */
	public static ListQueuesResult listQueues() {

		System.out.println("sqs.listQueues: " + sqs.listQueues());
		return sqs.listQueues();

	}

}
