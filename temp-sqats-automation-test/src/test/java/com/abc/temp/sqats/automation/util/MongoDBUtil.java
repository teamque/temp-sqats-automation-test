package com.abc.temp.sqats.automation.util;

import java.util.Map;
import java.util.NoSuchElementException;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

/**
 * Class provides methods to perform functionalities on the MongoDB database
 * 
 * @author hmalick
 *
 */
public class MongoDBUtil {

	private static final Logger log = LoggerFactory.getLogger(MongoDBUtil.class);

	/**
	 * Method used to find record by value from MongoDB database. Provide the
	 * database name, collection name, parameter name and parameter value
	 * 
	 * @param dbName
	 * @param dbCollection
	 * @param paramName
	 * @param paramValue
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public String find(String dbName, String dbCollection, String paramName, String paramValue) {

		String contents = "";
		try {

			String dbURI = ParameterDataSetup.mongodbconnURI;
			MongoClient mongoClient = new MongoClient(new MongoClientURI(dbURI));
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");

			DB databseName = mongoClient.getDB(dbName);
			DBCollection collection = databseName.getCollection(dbCollection);

			log.info("DatabaseName: " + databseName);
			log.info("DBCollection: " + collection);

			DBCursor cursor = collection.find();

			BasicDBObject query = new BasicDBObject(paramName, paramValue);
			BasicDBObject query2 = new BasicDBObject("$natural", -1);

			cursor = collection.find(query).sort(query2).limit(1);

			DBObject myDoc = collection.findOne(query);
			log.info("MongoDB Response: " + myDoc);

			contents = cursor.next().toString();

			log.info("MongoDB Response: " + contents);

			mongoClient.close();
		} catch (NoSuchElementException e) {
			log.error("Fail", e);
			contents = null;
		} catch (Exception e) {
			log.error("Fail", e);
			Assert.fail("Fail", e);
		}
		return contents;
	}

	/**
	 * Method used to find record by mongoId from MongoDB database. Provide the
	 * database name, collection name and parameter value
	 * 
	 * @param dbName
	 * @param dbCollection
	 * @param paramValue
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public String findById(String dbName, String dbCollection, String paramValue) {

		String contents = "";
		try {

			String dbURI = ParameterDataSetup.mongodbconnURI;
			log.info("dbURI: " + dbURI);
			MongoClient mongoClient = new MongoClient(new MongoClientURI(dbURI));
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");

			// Thread.sleep(2000);

			DB databseName = mongoClient.getDB(dbName);
			DBCollection collection = databseName.getCollection(dbCollection);

			log.info("DatabaseName: " + databseName);
			log.info("DBCollection: " + collection);
			log.info("paramValue: " + paramValue);

			DBCursor cursor = collection.find();

			// Thread.sleep(2000);

			BasicDBObject query = new BasicDBObject("_id", new ObjectId(paramValue));
			BasicDBObject query2 = new BasicDBObject("$natural", -1);

			cursor = collection.find(query).sort(query2).limit(1);

			DBObject myDoc = collection.findOne(query);
			log.info("MongoDB Response: " + myDoc);

			contents = cursor.next().toString();

			log.info("MongoDB Response: " + contents);

			mongoClient.close();
		} catch (NoSuchElementException e) {
			log.error("Fail", e);
			contents = null;
		} catch (Exception e) {
			log.error("Fail", e);
			Assert.fail("Fail", e);
		}
		return contents;
	}

	public String mongodbConnectSearchDBByValue(String dbName, String dbCollection, String dbId, String dbValue) {
		return mongodbConnectSearchDBByValue(dbName, dbCollection, dbId, dbValue, false);
	}

	@SuppressWarnings("deprecation")
	public String mongodbConnectSearchDBByValue(String dbName, String dbCollection, String dbId, String dbValue,
			Boolean isFulfillment) {
		return mongodbConnectSearchDBByValue(dbName, dbCollection, dbId, dbValue, isFulfillment, 40,
				ParameterDataSetup.mongodbconnURI);
	}

	@SuppressWarnings("deprecation")
	public String mongodbConnectSearchDBByValue(String dbName, String dbCollection, String dbId, String dbValue,
			Boolean isFulfillment, String mongoDBconnURI) {
		return mongodbConnectSearchDBByValue(dbName, dbCollection, dbId, dbValue, isFulfillment, 40, mongoDBconnURI);
	}

	/**
	 * Method used to find record by value from MongoDB database. Provide the
	 * database name, collection name, parameter id, parameter value, is
	 * fulfillment or not, number of tries and mongoDB connection uri
	 * 
	 * @param dbName
	 * @param dbCollection
	 * @param dbId
	 * @param dbValue
	 * @param isFulfillment
	 * @param tries
	 * @param mongoDBconnURI
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public String mongodbConnectSearchDBByValue(String dbName, String dbCollection, String dbId, String dbValue,
			Boolean isFulfillment, int tries, String mongoDBconnURI) {

		String contents = "";
		try {
			log.info("dbValue: " + dbValue);

			String dbURI = mongoDBconnURI;
			MongoClient mongoClient = new MongoClient(new MongoClientURI(dbURI));
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");

			DB databseName = mongoClient.getDB(dbName);
			DBCollection collection = databseName.getCollection(dbCollection);

			log.info("DatabaseName: " + databseName);
			log.info("DBCollection: " + collection);

			DBCursor cursor = collection.find();

			BasicDBObject query = new BasicDBObject(dbId, dbValue);
			BasicDBObject query2 = new BasicDBObject("$natural", -1);

			cursor = collection.find(query).sort(query2).limit(1);

			DBObject myDoc = null;
			int count = 0;
			if (isFulfillment == true) {
				while (myDoc == null) {
					log.info("GetRecord Attempt: " + count);
					myDoc = collection.findOne(query);
					Thread.sleep(1500);
					count++;
					if (count == tries) {
						Assert.assertNotNull(myDoc, "MongoDB Response");
						break;
					}
				}
			} else if (isFulfillment == false) {
				myDoc = collection.findOne(query);
			}
			log.info("MongoDB Response: " + myDoc);
			/* Assert.assertNotNull(myDoc, "MongoDB Response"); */

			if (null != myDoc) {
				contents = cursor.next().toString();
			} else {
				contents = null;
			}

			log.info("MongoDB Response: " + contents);

			mongoClient.close();
		} catch (Exception e) {
			log.error("Fail", e);
			Assert.fail("Fail", e);
		}
		return contents;
	}

	public String mongodbConnectSearchCompanyByObjectId(String dbName, String dbCollection, String dbId,
			String dbValue) {
		return mongodbConnectSearchCompanyByObjectId(dbName, dbCollection, dbId, dbValue, false);
	}

	/**
	 * Method used to find record by objectId from MongoDB database. Provide the
	 * database name, collection name, parameter id, parameter value and is
	 * fulfillment or not
	 * 
	 * @param dbName
	 * @param dbCollection
	 * @param dbId
	 * @param dbValue
	 * @param isFulfillment
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public String mongodbConnectSearchCompanyByObjectId(String dbName, String dbCollection, String dbId, String dbValue,
			Boolean isFulfillment) {

		String contents = "";
		try {

			String dbURI = ParameterDataSetup.mongodbconnURI;
			MongoClient mongoClient = new MongoClient(new MongoClientURI(dbURI));
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");

			DB databseName = mongoClient.getDB(dbName);
			DBCollection collection = databseName.getCollection(dbCollection);

			log.info("DatabaseName: " + databseName);
			log.info("DBCollection: " + collection);

			DBCursor cursor = collection.find();

			BasicDBObject query = new BasicDBObject(dbId, new ObjectId(dbValue));
			BasicDBObject query2 = new BasicDBObject("$natural", -1);

			cursor = collection.find(query).sort(query2).limit(1);

			DBObject myDoc = null;
			int count = 0;
			if (isFulfillment) {
				while (myDoc == null) {
					log.info("GetRecord Attempt: " + count);
					myDoc = collection.findOne(query);
					Thread.sleep(1500);
					count++;
					if (count == 40) {
						Assert.assertNotNull(myDoc, "MongoDB Response");
						break;
					}
				}
			} else {
				myDoc = collection.findOne(query);
			}
			log.info("MongoDB Response: " + myDoc);
			/* Assert.assertNotNull(myDoc, "MongoDB Response"); */

			if (null != myDoc) {
				contents = cursor.next().toString();
			} else {
				contents = null;
			}

			log.info("MongoDB Response: " + contents);

			mongoClient.close();
		} catch (Exception e) {
			log.error("Fail", e);
			Assert.fail("Fail", e);
		}
		return contents;
	}

	
	/**
	 * Method used to find record by value from MongoDB database. Provide the
	 * database name, collection name, parameter id, parameter value, is
	 * fulfillment or not, number of tries and mongoDB connection uri
	 * 
	 * @param dbName
	 * @param dbCollection
	 * @param dbObjIdMap
	 * @return
	 */
	public String mongodbConnectSearchDBMultipleValues(String dbName, String dbCollection,
			Map<String, Object> dbObjIdMap) {
		return mongodbConnectSearchDBMultipleValues(dbName, dbCollection, dbObjIdMap, false);
	}

	/**
	 * Method used to find record by multiple values from MongoDB database. Provide the
	 * database name, collection name, parameter id, parameter value, is
	 * fulfillment or not, number of tries and mongoDB connection uri
	 * 
	 * @param dbName
	 * @param dbCollection
	 * @param dbObjIdMap
	 * @param isFulfillment
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public String mongodbConnectSearchDBMultipleValues(String dbName, String dbCollection,
			Map<String, Object> dbObjIdMap, Boolean isFulfillment) {

		String contents = "";
		try {

			if (null != dbObjIdMap) {
				for (Map.Entry<String, Object> element : dbObjIdMap.entrySet()) {
					log.info("dbObjIdMap: Key is: " + element.getKey() + " & Value is: " + element.getValue() + "\n");
				}
			}

			String dbURI = ParameterDataSetup.mongodbconnURI;
			MongoClient mongoClient = new MongoClient(new MongoClientURI(dbURI));
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");

			DB databseName = mongoClient.getDB(dbName);
			DBCollection collection = databseName.getCollection(dbCollection);

			log.info("DatabaseName: " + databseName);
			log.info("DBCollection: " + collection);

			DBCursor cursor = collection.find();

			BasicDBObject query = new BasicDBObject(dbObjIdMap);
			BasicDBObject query2 = new BasicDBObject("$natural", -1);

			cursor = collection.find(query).sort(query2).limit(1);

			DBObject myDoc = null;
			int count = 0;
			if (isFulfillment) {
				while (myDoc == null) {
					log.info("GetRecord Attempt: " + count);
					myDoc = collection.findOne(query);
					Thread.sleep(1500);
					count++;
					if (count == 40) {
						Assert.assertNotNull(myDoc, "MongoDB Response");
						break;
					}
				}
			} else {
				myDoc = collection.findOne(query);
			}
			log.info("MongoDB Response: " + myDoc);
			Assert.assertNotNull(myDoc, "MongoDB Response");

			contents = cursor.next().toString();

			log.info("MongoDB Response: " + contents);

			mongoClient.close();
		} catch (Exception e) {
			log.error("Fail", e);
			Assert.fail("Fail", e);
		}
		return contents;
	}

	@SuppressWarnings("deprecation")
	public String mongodbConnectResponseByValue(String dbName, String dbCollection, String dbId, String dbValue) {
		return mongodbConnectResponseByValue(dbName, dbCollection, dbId, dbValue, false);
	}

	@SuppressWarnings("deprecation")
	public String mongodbConnectResponseByValue(String dbName, String dbCollection, String dbId, String dbValue,
			Boolean isFulfillment) {
		return mongodbConnectResponseByValue(dbName, dbCollection, dbId, dbValue, isFulfillment, 40);
	}

	/**
	 * Method used to find record by value from MongoDB database. Provide the
	 * database name, collection name, parameter id, parameter value, is
	 * fulfillment or not, number of tries and mongoDB connection uri
	 * 
	 * @param dbName
	 * @param dbCollection
	 * @param dbId
	 * @param dbValue
	 * @param isFulfillment
	 * @param tries
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public String mongodbConnectResponseByValue(String dbName, String dbCollection, String dbId, String dbValue,
			Boolean isFulfillment, int tries) {

		String contents = "";
		try {
			log.info("dbValue: " + dbValue);

			String dbURI = ParameterDataSetup.mongodbconnURI;
			MongoClient mongoClient = new MongoClient(new MongoClientURI(dbURI));
			log.info("Connected to " + ParameterDataSetup.testEnvironment + " database.");

			DB databseName = mongoClient.getDB(dbName);
			DBCollection collection = databseName.getCollection(dbCollection);

			log.info("DatabaseName: " + databseName);
			log.info("DBCollection: " + collection);

			DBCursor cursor = collection.find();

			BasicDBObject query = new BasicDBObject(dbId, dbValue);
			BasicDBObject query2 = new BasicDBObject("$natural", -1);

			cursor = collection.find(query).sort(query2).limit(1);

			DBObject myDoc = null;
			int count = 0;
			if (isFulfillment == true) {
				while (myDoc == null) {
					log.info("GetRecord Attempt: " + count);
					myDoc = collection.findOne(query);
					Thread.sleep(1500);
					count++;
					if (count == tries) {
						Assert.assertNotNull(myDoc, "MongoDB Response");
						break;
					}
				}
			} else if (isFulfillment == false) {
				myDoc = collection.findOne(query);
			}
			log.info("MongoDB Response: " + myDoc);

			if (null != myDoc) {
				contents = cursor.next().toString();
			} else {
				contents = null;
			}

			log.info("MongoDB Response: " + contents);

			mongoClient.close();
		} catch (Exception e) {
			log.error("Fail", e);
			Assert.fail("Fail", e);
		}
		return contents;
	}
}
