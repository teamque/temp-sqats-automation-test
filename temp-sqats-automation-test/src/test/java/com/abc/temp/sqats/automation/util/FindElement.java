package com.abc.temp.sqats.automation.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Class is a base util class used to find elements for Selenium Webdriver Test/s
 * 
 * @author hmalick
 * 
 */
public class FindElement extends BaseUtil {

	/**
	 * Used to find an element by Name and enter text
	 * 
	 * @param driver
	 *            Always Required
	 * @param name
	 *            Name of the element
	 * @param txt
	 *            Text to enter
	 */
	public void EnterTextByName(WebDriver driver, String name, String txt) {

		driver.findElement(By.name(name)).sendKeys(txt);
	}

	/**
	 * Used to find element by Id
	 * 
	 * @param driver
	 *            Always Required
	 * @param id
	 *            Id of the element
	 */
	public void findById(WebDriver driver, String id) {

		driver.findElement(By.id(id));
	}

	/**
	 * Used to find element by XPath
	 * 
	 * @param driver
	 *            Always Required
	 * @param xpath
	 *            XPath of element
	 */
	public void findByXpath(WebDriver driver, String xpath) {

		driver.findElement(By.xpath(xpath));
	}

	/**
	 * Used to find element by Link text
	 * 
	 * @param driver
	 *            Always Required
	 * @param link_text
	 *            Link text of element
	 */
	public void ClickByLinkText(WebDriver driver, String link_text) {

		driver.findElement(By.linkText(link_text)).click();
	}

}
