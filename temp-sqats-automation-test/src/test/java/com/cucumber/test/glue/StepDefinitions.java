package com.cucumber.test.glue;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.lang.reflect.Method;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.Parameters;

import com.abc.temp.sqats.automation.page.objects.SearchPage;
import com.abc.temp.sqats.automation.tests.SearchTest;
import com.abc.temp.sqats.automation.util.FileHandlerUtil;
import com.abc.temp.sqats.automation.util.ParameterDataSetup;
import com.abc.temp.sqats.automation.util.SetBrowser;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

/**
 * Class utilizes the Cucumber step definition annotations to run specific steps
 * for the Cucumber Test/s
 * 
 * @author hmalick
 *
 */
public class StepDefinitions extends SearchTest {

	private static final Logger log = LoggerFactory.getLogger(StepDefinitions.class);
	
	JsonPath testData = null;

	SearchPage searchPage;
	
	@Before
	public void beforeScenario() throws Exception {
		log.info("Starting Test Class");
		SetBrowser set = new SetBrowser();
		driver = set.setBrowser(ParameterDataSetup.testBrowser);
		browser = ParameterDataSetup.testBrowser;
		driver.manage().window().maximize();
		
		testData = FileHandlerUtil.getFileJsonPathFromTestResource(ParameterDataSetup.testDataFilePath + "search/search.json");
	}

	/**
	 * Given is the pre-condition step
	 * 
	 * @param service
	 * @throws Exception 
	 */
	@Given("^I navigate to the \"([^\"]*)\" page with the url$")
	public void I_have_the_search_information_to_perform_a_search(String search) {
		try {
			searchPage = new SearchPage(driver);

			searchPage.searchUrl();

		} catch (NoSuchElementException e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		} catch (Exception e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		}
	}

	/**
	 * When is the action step
	 * 
	 * @param call
	 */
	@When("^I perform the \"([^\"]*)\" using the search data$")
	public void I_perform_the_search_using_the_search_data(String search) {
		try {
			searchPage.searchGoogle(testData);

		} catch (NoSuchElementException e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		} catch (Exception e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		}
	}

	/**
	 * Then is the validation step
	 * 
	 * @param status
	 */
	@Then("^The \"([^\"]*)\" should be successful$")
	public void The_search_should_be_successful(String search) {

	}

	@Then("^The search result should be displayed$")
	public void The_search_result_should_be_displayed() {
		try {
			searchPage.validateSearchResult(testData);
		} catch (NoSuchElementException e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		} catch (Exception e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		}
	}

	@After
	public void afterScenario() {
		try {
			driver.quit();

		} catch (Exception ex) {
			log.error("Failed", ex);
		}
	}
}