package com.abc.temp.sqats.automation.util;

import org.openqa.selenium.WebDriver;

/**
 * Class is an abstract base util class that is used by Test/s
 * 
 * @author hmalick
 *
 */
public class BaseUtil {

	private WebDriver driver;

	/**
	 * Method sets the driver for Selenium WebDriver
	 * 
	 * @return driver
	 */
	public WebDriver getDriver() {
		return driver;
	}

}
