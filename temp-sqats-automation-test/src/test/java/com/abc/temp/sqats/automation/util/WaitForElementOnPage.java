package com.abc.temp.sqats.automation.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Class provides methods to wait for elements on a page
 * 
 * @author hmalick
 *
 */
public class WaitForElementOnPage extends BaseUtil {

	/**
	 * Method is used for verifying page has loaded by waiting for a specific
	 * Xpath that can be clicked on the page. Timeout is at 20 seconds.
	 * 
	 * @param driver
	 *            Always Required
	 * @param xpath
	 *            Xpath of element on page
	 */
	public void waitForXpath(WebDriver driver, String xpath) {
		/*
		 * Used mostly for verifying correct page has loaded This method waits
		 * for an xpath to be clicked Will time out after 20 seconds if element
		 * not found
		 */

		waitForElementToBeClickable(driver, By.xpath(xpath));
	}

	/**
	 * Method is used for verifying page has loaded by waiting for a specific
	 * Link Text that can be clicked on the page. Timeout is at 20 seconds.
	 * 
	 * @param driver
	 *            Always Required
	 * @param link_text
	 *            Specific link text of element on page
	 */
	public void waitForLink(WebDriver driver, String link_text) {
		/*
		 * Used mostly for verifying correct page has loaded This method waits
		 * for a link (must specify link text) to be clicked Will time out after
		 * 20 seconds if element not found
		 */

		waitForElementToBeClickable(driver, By.linkText(link_text));
	}

	/**
	 * Method is used for verifying page has loaded by waiting for partial Link
	 * Text that can be clicked on the page. Timeout is at 20 seconds.
	 * 
	 * @param driver
	 *            Always Required
	 * @param partial_link_text
	 *            Parital link text of element on page
	 */
	public void waitForPartialLink(WebDriver driver, String partial_link_text) {
		/*
		 * Used mostly for verifying correct page has loaded This method waits
		 * for a link (only needs partial link text) to be clicked Will time out
		 * after 20 seconds if element not found
		 */

		waitForElementToBeClickable(driver, By.partialLinkText(partial_link_text));
	}

	/**
	 * Method is used for verifying page has loaded by waiting for a button by
	 * its name that can be clicked on the page. Timeout is at 20 seconds.
	 * 
	 * @param driver
	 * @param name
	 */
	public void waitForButton(WebDriver driver, String name) {
		/*
		 * Used mostly for verifying correct page has loaded This method waits
		 * for a button to be clicked by button name Will time out after 20
		 * seconds if element not found
		 */

		waitForElementToBeClickable(driver, By.name(name));
	}

	/**
	 * Method is used for verifying page has loaded by waiting for a CSS button
	 * by its cssSelector that can be clicked on the page. Timeout is at 20
	 * seconds.
	 * 
	 * @param driver
	 * @param name
	 */
	public void waitForcssButton(WebDriver driver, String css) {
		/*
		 * Used mostly for verifying correct page has loaded This method waits
		 * for a CSS button to be clicked by button cssSelector Will time out
		 * after 20 seconds if element not found
		 */

		waitForElementToBeClickable(driver, By.cssSelector(css));
	}

	/**
	 * Method is used for verifying page has loaded by waiting for a specific
	 * Xpath that can be clicked on the page. Timeout is at 20 seconds.
	 * 
	 * @param driver
	 *            Always Required
	 * @param xpath
	 *            Xpath of element on page
	 */
	public void waitForId(WebDriver driver, String id) {
		/*
		 * Used mostly for verifying correct page has loaded This method waits
		 * for an xpath to be clicked Will time out after 20 seconds if element
		 * not found
		 */

		waitForPresenceOfElementLocated(driver, By.id(id));

	}

	/**
	 * Method is used for verifying page has loaded by waiting for a button by
	 * its name that can be clicked on the page. Timeout is at 20 seconds.
	 * 
	 * @param driver
	 * @param name
	 */
	public void waitForName(WebDriver driver, String name) {
		/*
		 * Used mostly for verifying correct page has loaded This method waits
		 * for a button to be clicked by button name Will time out after 20
		 * seconds if element not found
		 */

		waitForPresenceOfElementLocated(driver, By.name(name));
	}

	/**
	 * Method is used for verifying page has loaded by waiting for a specific
	 * Xpath that can be clicked on the page. Timeout is at 20 seconds.
	 * 
	 * @param driver
	 *            Always Required
	 * @param xpath
	 *            Xpath of element on page
	 */
	public void waitForElementbyXpath(WebDriver driver, String xpath) {
		/*
		 * Used mostly for verifying correct page has loaded This method waits
		 * for an xpath to be clicked Will time out after 20 seconds if element
		 * not found
		 */

		waitForPresenceOfElementLocated(driver, By.xpath(xpath));

	}

	/**
	 * Method is used for verifying page has loaded by waiting for a specific
	 * Xpath that can be clicked on the page. Timeout is at 20 seconds.
	 * 
	 * @param driver
	 *            Always Required
	 * @param xpath
	 *            Xpath of element on page
	 */
	public void waitForElementbyCSS(WebDriver driver, String css) {
		/*
		 * Used mostly for verifying correct page has loaded This method waits
		 * for an xpath to be clicked Will time out after 20 seconds if element
		 * not found
		 */

		waitForPresenceOfElementLocated(driver, By.cssSelector(css));
	}

	/**
	 * Method is used to wait for an alert to be displayed. Timeout is at 20
	 * seconds.
	 * 
	 * @param driver
	 */
	public void waitForAlert(WebDriver driver) {

		WebDriverWait waitUntil = new WebDriverWait(driver, 5);
		waitUntil.until(ExpectedConditions.alertIsPresent());
	}

	/**
	 * Method is used to wait for an element to be located by either id, name,
	 * xpath etc
	 * 
	 * @param driver
	 * @param param
	 */
	public void waitForPresenceOfElementLocated(WebDriver driver, By param) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.presenceOfElementLocated(param));
		} catch (Exception e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		}

	}

	/**
	 * Method is used to wait for an element to be click able
	 * 
	 * @param driver
	 * @param param
	 */
	public void waitForElementToBeClickable(WebDriver driver, By param) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.elementToBeClickable(param));
		} catch (Exception e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		}
	}
}
