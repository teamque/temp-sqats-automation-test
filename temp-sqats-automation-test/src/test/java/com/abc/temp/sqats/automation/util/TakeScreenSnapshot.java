package com.abc.temp.sqats.automation.util;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

/**
 * Class provides methods to take screen shots
 * 
 * @author hmalick
 *
 */
public class TakeScreenSnapshot {

	private static final Logger log = LoggerFactory.getLogger(TakeScreenSnapshot.class);
	// screen capture will be saved to the given folder when the test is
	// complete
	private String screen_capture_folder = ParameterDataSetup.testScreenShotPath;

	/**
	 * Method saves the screen capture taken after the completion or failure of
	 * the test case
	 * 
	 * @param driver
	 *            Always Required
	 * @param prefix
	 *            Test Name
	 */
	public void GetScreenSnapshot(WebDriver driver, String prefix) {
		try {
			// take the screenshot of the last screen at the completion (or
			// after failure) of the test case
			File screencapture = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			DateTimeStamp time_stamp = new DateTimeStamp();
			prefix = prefix.replaceAll(" ", "");

			// save the screenshot to the location specified below
			String screencapturefile = screen_capture_folder + prefix + "_" + time_stamp.GetTimeStampValue()
					+ "_screenshot.jpg";
			log.info("A snapshot of last encountered screen is available at: \"" + screencapturefile + "\" \n \n");
			FileUtils.copyFile(screencapture, new File(screencapturefile));
		} catch (Exception e) {
			log.error("Could not create a snapshot file of the last screen.");
			log.error("Fail ", e);
			Assert.fail("Could not create a snapshot file of the last screen.", e);
		}
	}
}
