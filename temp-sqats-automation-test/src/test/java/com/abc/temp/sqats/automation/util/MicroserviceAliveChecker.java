package com.abc.temp.sqats.automation.util;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

public class MicroserviceAliveChecker extends APIBaseTestUtil {

	public MicroserviceAliveChecker() {
		super(log);
	}
	
	private static final Logger log = LoggerFactory
			.getLogger(MicroserviceAliveChecker.class);

	private List<APIMiceroservicesEnum> microservices = new ArrayList<APIMiceroservicesEnum>();
	private Map<APIMiceroservicesEnum, Boolean> microservicesAlive = new HashMap<APIMiceroservicesEnum, Boolean>();
	
	public List<APIMiceroservicesEnum> getMicroservices() {
		return microservices;
	}

	public void setMicroservices(List<APIMiceroservicesEnum> microservices) {
		this.microservices = microservices;
	}

	public Map<APIMiceroservicesEnum, Boolean> getMicroservicesAlive() {
		return microservicesAlive;
	}

	public void setMicroservicesAlive(Map<APIMiceroservicesEnum, Boolean> microservicesAlive) {
		this.microservicesAlive = microservicesAlive;
	}

	public void addMiceroservice(APIMiceroservicesEnum miceroservice){
		if( null==microservices ){
			microservices = new ArrayList<APIMiceroservicesEnum>();
		}
		microservices.add(miceroservice);
	}
	
	public void clearMiceroservice(){
		microservices.clear();
	}
	
	public boolean check(){
		try {
			microservicesAlive = isAlive(microservices);
			boolean isAllAlive = true;
			for (Map.Entry<APIMiceroservicesEnum, Boolean> element : microservicesAlive.entrySet()) {
				isAllAlive &= element.getValue();
			}
			return isAllAlive;
		} catch (Exception e) {
			log.error("Error isAlive check for microservice: "+microservices, e);
			return false;
		}
	}
	
	public void print(){
		StringBuilder strb = new StringBuilder();
		strb.append("\nIs Alive Check...");
		for (Map.Entry<APIMiceroservicesEnum, Boolean> element : microservicesAlive.entrySet()) {
			strb.append("\n "+element.getKey().getName() + " :  " + element.getValue());
		}
		strb.append("\n");
		log.info(strb.toString());
	}
	

	public Map<APIMiceroservicesEnum, Boolean> isAlive(List<APIMiceroservicesEnum> miceroservices) throws Exception {
		Map<APIMiceroservicesEnum, Boolean> microservices = new HashMap<APIMiceroservicesEnum, Boolean>();
		for (APIMiceroservicesEnum apiMiceroservicesEnum : miceroservices) {
			microservices.put(apiMiceroservicesEnum, false);
			try {
				boolean isAlive = isAlive(apiMiceroservicesEnum);
				microservices.put(apiMiceroservicesEnum, isAlive);
			} catch (Exception e) {
				log.error("Error isAlive check for microservice: "+apiMiceroservicesEnum, e);
			}
		}
		return microservices;
	}

	public boolean isAlive(APIMiceroservicesEnum miceroservices) throws Exception {
		String url = null;

		MicConfiguration micConfig = null;
		
		if( miceroservices.equals(APIMiceroservicesEnum.ACCOUNT) ){
			micConfig = APIConfiguration.micAccount;
		} else if( miceroservices.equals(APIMiceroservicesEnum.CANDIDATE) ){
			micConfig = APIConfiguration.micCandidate;
		} else if( miceroservices.equals(APIMiceroservicesEnum.COMPANY) ){
			micConfig = APIConfiguration.micCompany;
		} else if( miceroservices.equals(APIMiceroservicesEnum.FULFILLMENT) ){
			micConfig = APIConfiguration.micFulfillment;
		} else if( miceroservices.equals(APIMiceroservicesEnum.ORDER) ){
			micConfig = APIConfiguration.micOrder;
		} else if( miceroservices.equals(APIMiceroservicesEnum.REPORT) ){
			micConfig = APIConfiguration.micReport;
		}  else if( miceroservices.equals(APIMiceroservicesEnum.PACKAGE) ){
			micConfig = APIConfiguration.micPackage;
		} else if( miceroservices.equals(APIMiceroservicesEnum.PROXY) ){
			micConfig = APIConfiguration.micProxy;
		} else if( miceroservices.equals(APIMiceroservicesEnum.SECURITY) ){
			micConfig = APIConfiguration.micSecurity;
		} else if( miceroservices.equals(APIMiceroservicesEnum.STATUS_CHANGE) ){
			micConfig = APIConfiguration.micStatusChange;
		}

		//setBaseUrl(micConfig.getMicBaseUrl());
		setMicConfiguration(micConfig);
		url = micConfig.getMicBaseUrl()+"/alive";
		/*
		if( miceroservices.equals(APIMiceroservicesEnum.SECURITY) ){
			url = micConfig.getMicBaseUrl()+"/creds/alive";
		} else {
			url = micConfig.getMicBaseUrl()+"/alive";
		}
		*/
		
		RequestSpecification request = given()
				.log().all()
				.contentType(HttpMethod.GET.toString());
		Response response = request
				.when()
				.get(url);
		
		return response.getStatusCode()==200 ? true : false; 
		
	}



}
