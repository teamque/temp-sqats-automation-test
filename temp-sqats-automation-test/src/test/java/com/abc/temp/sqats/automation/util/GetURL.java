package com.abc.temp.sqats.automation.util;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

/**
 * Class is a base util class used to get URL for Selenium Webdriver Test/s
 * 
 * @author hmalick
 *
 */
public class GetURL extends BaseUtil {


	private static final Logger log = LoggerFactory.getLogger(GetURL.class);

	/**
	 * Sets the URL for Google Search
	 * 
	 * @param driver
	 *            Always required
	 * @param environment
	 *            Set the environment on which the test will be executed
	 */
	public void setSearchURL(WebDriver driver, String environment) {

		String searchUrl=ParameterDataSetup.testParameters.getString("searchUrl");
		driver.get(searchUrl);

	}
}