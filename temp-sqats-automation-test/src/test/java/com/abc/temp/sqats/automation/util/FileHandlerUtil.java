package com.abc.temp.sqats.automation.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.path.json.JsonPath;

/**
 * Class is a base util class used to read file content
 * 
 * @author hmalick
 *
 */
public class FileHandlerUtil {
		
		private static final Logger log = LoggerFactory
			.getLogger(FileHandlerUtil.class);
		
		public static final String TEST_RESOURCE_FOLDER = "src/test/resources/";
		
		/*public static JsonNode getFileJsonNodeFromTestResource(String filename) throws Exception {
			log.info("Getting test data from file: "+filename);
			String content = FileHandlerUtil.getFileJsonContentFromTestResource(filename);
			System.out.println(content);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode root = mapper.readTree(content);
			return root;
		}*/
		
		public static JsonPath getFileJsonPathFromTestResource(String filename) throws Exception {
			log.info("Getting test data from file: "+filename);
			String content = FileHandlerUtil.getFileJsonContentFromTestResource(filename);
			System.out.println(content);
			
			JsonPath root=new JsonPath(content);
			return root;
		}
		
		/**================= Get content of file =================*/
		public static String getFileJsonContentFromTestResource(String filename) throws Exception {
			return new String(Files.readAllBytes(Paths.get(TEST_RESOURCE_FOLDER + filename)));
		}
		
		public static String getFileContentFromTestResource(String filename) throws Exception {
			return new String(Files.readAllBytes(Paths.get(TEST_RESOURCE_FOLDER + filename)));
		}

		public static String getFileContent(String filenameWithPath) throws Exception {
			return new String(Files.readAllBytes(Paths.get(filenameWithPath)));
		}
		
		

		/**================= Get content of file as Properties =================*/
		public static Properties getParamsFromTestResource(String filename) throws Exception {
			Properties prop = new Properties();
			InputStream input = null;
			input = new FileInputStream(TEST_RESOURCE_FOLDER + "params/" + filename);
			prop.load(input);
			return prop;
		}
		
		public static Properties getPropertiesFromTestResource(String filename) throws Exception {
			Properties prop = new Properties();
			InputStream input = null;
			input = new FileInputStream(TEST_RESOURCE_FOLDER + filename);
			prop.load(input);
			return prop;
		}

		public static Properties getProperties(String filenameWithPath) throws Exception {
			Properties prop = new Properties();
			InputStream input = null;
			input = new FileInputStream(filenameWithPath);
			prop.load(input);
			return prop;
		}
		
	}


