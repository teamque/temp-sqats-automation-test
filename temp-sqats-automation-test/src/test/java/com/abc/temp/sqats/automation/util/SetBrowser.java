package com.abc.temp.sqats.automation.util;

import java.net.MalformedURLException;
import java.net.URL;

/*import net.anthavio.phanbedder.Phanbedder;*/

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/*import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;*/

/**
 * Class provides methods to set the browser for the Test/s
 * 
 * @author hmalick
 *
 */
public class SetBrowser extends BaseUtil {

	private static final Logger log = LoggerFactory.getLogger(SetBrowser.class);

	/**
	 * Method is used to set up the browser to run test/s with either Firefox,
	 * Chrome or Internet Explorer
	 * 
	 * @param browser
	 *            TODO Select browsers (firefox, chrome, iexplorer, firefoxgrid,
	 *            chromegrid, iexplorergrid)
	 * @return Browser driver
	 * @throws MalformedURLException
	 */
	public WebDriver setBrowser(String browser) throws MalformedURLException {

		WebDriver driver = null;

		if (browser.equals("firefox")) {

			driver = new FirefoxDriver();
			log.info("Running Test on Firefox \n");

		} else if (browser.equals("iexplorer")) {

			driver = new InternetExplorerDriver();
			log.info("Running Test on InternetExplorer \n");

		} else if (browser.equals("chrome")) {

			System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");

			driver = new ChromeDriver();
			log.info("Running Test on Chrome \n");

		} else if (browser.equals("firefoxgrid")) {

			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setBrowserName("firefox");
			cap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);

			driver = new RemoteWebDriver(new URL(ParameterDataSetup.testGridServer), cap);
			log.info("Running Test on Firefox \n");

		} else if (browser.equals("iexplorergrid")) {

			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setBrowserName("iexplorer");
			cap.setPlatform(org.openqa.selenium.Platform.WINDOWS);
			cap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);

			driver = new RemoteWebDriver(new URL(ParameterDataSetup.testGridServer), cap);
			log.info("Running Test on InternetExplorer \n");

		} else if (browser.equals("chromegrid")) {

			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setBrowserName("chrome");
			cap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);

			driver = new RemoteWebDriver(new URL(ParameterDataSetup.testGridServer), cap);
			log.info("Running Test on Chrome \n");

		} /*
			 * else if (browser.equals("phantomjs")) { File phantomjs =
			 * Phanbedder.unpack(); DesiredCapabilities dcap = new
			 * DesiredCapabilities(); dcap.setCapability(PhantomJSDriverService.
			 * PHANTOMJS_EXECUTABLE_PATH_PROPERTY, phantomjs.getAbsolutePath());
			 * dcap.setPlatform(org.openqa.selenium.Platform.WINDOWS);
			 * 
			 * dcap.setJavascriptEnabled(true);
			 * 
			 * driver = new PhantomJSDriver(dcap);
			 * 
			 * driver = new RemoteWebDriver(new
			 * URL(ParameterDataSetup.testGridServer), dcap.phantomjs());
			 * 
			 * log.info("Running Test on PhantomJS \n");
			 * 
			 * }
			 */ else if (browser.equals("browserstack_ie11")) {

			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability("browserstack.local", "true");
			cap.setCapability("browserstack.debug", "true");
			cap.setCapability("browser", "IE");
			cap.setCapability("browser_version", "11.0");
			cap.setCapability("os", "Windows");
			cap.setCapability("os_version", "8.1");
			cap.setCapability("resolution", "1920x1080");
			cap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);

			driver = new RemoteWebDriver(
					new URL("http://harithmalick:VvyWpwbqz5o6nSHqYyPN@hub.browserstack.com/wd/hub"), cap);
			log.info("Running Test on Firefox \n");

			/* WebDriver driver = new FirefoxDriver(); */

		} else if (browser.equals("browserstack_safari8")) {

			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability("browserstack.local", "true");
			cap.setCapability("browserstack.debug", "true");
			cap.setCapability("browser", "Safari");
			cap.setCapability("browser_version", "8.0");
			cap.setCapability("os", "OS X");
			cap.setCapability("os_version", "Yosemite");
			cap.setCapability("resolution", "1920x1080");
			cap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);

			driver = new RemoteWebDriver(
					new URL("http://harithmalick:VvyWpwbqz5o6nSHqYyPN@hub.browserstack.com/wd/hub"), cap);
			log.info("Running Test on Firefox \n");

		} else {

			log.info("Browser is not defined");

		}

		return driver;

	}

}
