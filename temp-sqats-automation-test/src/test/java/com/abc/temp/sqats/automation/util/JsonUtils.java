package com.abc.temp.sqats.automation.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class is a base util class used to read json response from RESTful API Test/s
 * 
 * @author hmalick
 *
 */
public class JsonUtils {
	
	private final static Logger logger = LoggerFactory.getLogger(JsonUtils.class);
	
	/*public static <T> T parse(String data, Class<?> target) throws RequestParserException {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
	        return objectMapper.readValue(data, objectMapper.getTypeFactory().constructType(Class.forName(target.getName())));
		} catch (JsonParseException e) {
			//logger.error("parse", e);
			throw new RequestParserException("invalid json", e);
		} catch (JsonMappingException e) {
			//logger.error("parse", e);
			throw new RequestParserException("invalid json", e);
		} catch (IOException e) {
			//logger.error("parse", e);
			throw new RequestParserException("invalid json", e);
		} catch (ClassNotFoundException e) {
			//logger.error("parse", e);
			throw new RequestParserException("invalid json", e);
		}
	}
	
	
	public static String getBody(HttpServletRequest request) throws RequestParserException {

	    String body = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    BufferedReader bufferedReader = null;

	    try {
	        InputStream inputStream = request.getInputStream();
	        if (inputStream != null) {
	            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	            char[] charBuffer = new char[128];
	            int bytesRead = -1;
	            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	                stringBuilder.append(charBuffer, 0, bytesRead);
	            }
	        } else {
	            stringBuilder.append("");
	        }
	    } catch (IOException ex) {
			//logger.error("getBody", ex);
			throw new RequestParserException("invalid request data", ex);
	    } finally {
	        if (bufferedReader != null) {
	            try {
	                bufferedReader.close();
	            } catch (IOException ex) {
	    			//logger.error("getBody", ex);
	    			throw new RequestParserException("invalid request data", ex);
	            }
	        }
	    }

	    body = stringBuilder.toString();
	    return body;
	}*/
	
}
