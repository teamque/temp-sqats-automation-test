package com.abc.temp.sqats.automation.util;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * Class provides methods to test messages from RabbitMQ
 * 
 * @author hmalick
 *
 */
public class RabbitMQUtil {

	private static String rabbitHost = "http://ld01-rabbitmq-c01.accuratebackground.int:15672/#/queues";
	private static String rabbitUsername = "apiv3";
	private static String rabbitPassword = "ap1v3r@661tm9";
	private static String rabbitVirtualHost = "dev-core-services";
	private static String rabbitQueue = "phx-ds-company-event-queue";

	public void getRabbitMQMessage() {
		getRabbitMQMessage(rabbitQueue);
	}

	/**
	 * Method used to get a message from RabbitMQ. Provide the rabbit queue name
	 * 
	 * @param rabbitQueue
	 */
	public void getRabbitMQMessage(String rabbitQueue) {
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(rabbitHost);
			factory.setUsername(rabbitUsername);
			factory.setPassword(rabbitPassword);
			factory.setVirtualHost(rabbitVirtualHost);

			System.out.println("ConnectionFactory: " + factory.getHost());
			System.out.println("ConnectionFactory: " + factory.getPort());
			System.out.println("ConnectionFactory: " + factory.getUsername());
			System.out.println("ConnectionFactory: " + factory.getPassword());

			Connection connection = factory.newConnection();

			System.out.println("RabbitMQ Connection: " + factory.toString());

			Channel channel = connection.createChannel();

			channel.messageCount(rabbitQueue);

			System.out.println("RabbitMQ Channel: " + channel.toString());

			System.out.println("RabbitMQ Message: " + channel.messageCount(rabbitQueue));

			channel.close();

		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * public static void main(String[] argv) { try { ConnectionFactory factory
	 * = new ConnectionFactory();
	 * factory.setHost("ld00-rabbitmq-i01.accuratebackground.local");
	 * //factory.setPort(5672); factory.setUsername("phx");
	 * factory.setPassword("phx123"); //factory.useSslProtocol();
	 * factory.setVirtualHost("dev-phx");
	 * 
	 * System.out.println("ConnectionFactory: "+factory.getHost());
	 * System.out.println("ConnectionFactory: "+factory.getPort());
	 * System.out.println("ConnectionFactory: "+factory.getUsername());
	 * System.out.println("ConnectionFactory: "+factory.getPassword());
	 * 
	 * Connection connection = factory.newConnection();
	 * 
	 * System.out.println("RabbitMQ Connection: "+factory.toString());
	 * 
	 * Channel channel = connection.createChannel();
	 * 
	 * channel.messageCount("phx-ds-company-event-queue");
	 * 
	 * System.out.println("RabbitMQ Channel: "+channel.toString());
	 * 
	 * System.out.println("RabbitMQ Message: "+channel.messageCount(
	 * "phx-ds-company-event-queue"));
	 * 
	 * channel.close();
	 * 
	 * } catch (IOException e) { // Auto-generated catch block
	 * e.printStackTrace(); } catch (TimeoutException e) { // Auto-generated
	 * catch block e.printStackTrace(); }
	 * 
	 * }
	 */

}
