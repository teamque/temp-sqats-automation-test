package com.abc.temp.sqats.automation.util;

/**
 * Class defines the getters and setters for a specific micro service using the
 * RESTful API
 * 
 * @author hmalick
 *
 */
public class MicConfiguration {

	private String micDomain;
	private Integer micPort;
	private String micBaseUrl;

	public String getMicDomain() {
		return micDomain;
	}

	public void setMicDomain(String micDomain) {
		this.micDomain = micDomain;
	}

	public Integer getMicPort() {
		return micPort;
	}

	public void setMicPort(Integer micPort) {
		this.micPort = micPort;
	}

	public String getMicBaseUrl() {
		return micBaseUrl;
	}

	public void setMicBaseUrl(String micBaseUrl) {
		this.micBaseUrl = micBaseUrl;
	}

	public String makeUrl() {
		if (null != micPort && micPort.intValue() > 0) {
			return micDomain + ":" + micPort + micBaseUrl;
		} else {
			return micDomain + micBaseUrl;
		}
	}

	@Override
	public String toString() {
		return "MicConfiguration [micDomain=" + micDomain + ", micPort=" + micPort + ", micBaseUrl=" + micBaseUrl
				+ ", makeUrl=" + makeUrl() + "]";
	}

}
