package com.abc.temp.sqats.automation.util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

/**
 * Class provides methods to click links and utilizes the base utils
 * 
 * @author hmalick
 *
 */
public class ClickLink extends BaseUtil {

	private static final Logger log = LoggerFactory.getLogger(ValidatePage.class);
	int errorCode = 0;
	String errorString = "Hello";

	/**
	 * Method used to click links by text. Provide the driver for Selenium,
	 * description of link and text of link to be clicked
	 * 
	 * @param driver
	 * @param Link_Info
	 * @param Link_Text
	 */
	public void clickLink(WebDriver driver, String Link_Info, String Link_Text) {

		try {
			Boolean textFound = true;
			String delimiter = "[ ]+";
			String[] textArray = Link_Text.split(delimiter);

			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			// Is the words within the Link_Text are available on the page? If
			// so, set textFound to true
			for (int i = 0; i < textArray.length; i++)
				if (driver.getPageSource().contains(textArray[i])) {
					textFound = true;
				} else {
					textFound = false;
					break;
				}

			if (textFound) {

				log.info("Clicking the " + Link_Info + ".");
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(By.linkText(Link_Text)).click();
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			} else if ((driver.getPageSource().contains("Found following error:"))
					&& (driver.getPageSource().contains("Error"))) {

				errorCode = driver.getPageSource().indexOf("Error Code :");
				errorString = driver.getPageSource().subSequence(errorCode, 36).toString();
				log.info("Could not find: " + Link_Text);
				log.info("Instead encountered a server error message with the " + errorString);

				log.info("Click on the Back button to get out of error screen.");
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(By.xpath("//button[contains(.,'Back')]")).click();

			} else {

				log.error("Unable to locate or click the " + Link_Info + " in the page with tile "
						+ driver.getTitle().toString() + " and URL: " + driver.getCurrentUrl() + "\n");
				Assert.fail();

			}

		} catch (Exception e) {

			log.error("Unable to locate or click the " + Link_Info + ".\n");
			log.error("Fail ", e);
			Assert.fail();
		}

	}

}