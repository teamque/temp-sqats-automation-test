package com.abc.temp.sqats.automation.util;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

import com.jayway.restassured.path.json.JsonPath;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

/**
 * Class provides methods to perform functionalities on the MongoDB database
 * 
 * @author hmalick
 *
 */
public class ConnectMongoDBUtil {

	/**
	 * Method is the main method used to test the MongoDB database connection
	 * 
	 * @param args
	 * @throws UnknownHostException
	 */
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws UnknownHostException {
		String dbURI = "mongodb://10.34.180.56:27017/CompanyDB";
		MongoClient mongoClient = new MongoClient(new MongoClientURI(dbURI));

		DB dbName = mongoClient.getDB("CompanyDB");

		DBCollection collection = dbName.getCollection("company");

		System.out.println("- Database: " + dbName);
		System.out.println("\t + Collection: " + collection);

		DBCursor cursor = collection.find();
		try {
			while (cursor.hasNext()) {
				System.out.println(cursor.next());
			}
		} finally {
			cursor.close();
		}

		/*
		 * BasicDBObject query = new BasicDBObject("age", new
		 * BasicDBObject("$gt", 40));
		 */
		BasicDBObject query = new BasicDBObject("name", "ABC");
		BasicDBObject query2 = new BasicDBObject("$natural", -1);

		DBObject myDoc = collection.findOne(query);
		System.out.println("Highest scoring document: " + myDoc);

		JsonPath jsonPath = new JsonPath(myDoc.toString());
		String compName = jsonPath.getString("name");
		String compPhone = jsonPath.getString("phone");

		System.out.println("Company Name: " + compName);
		System.out.println("Company Phone: " + compPhone);

		cursor = collection.find(query).sort(query2).limit(1);
		/* System.out.println("Person with age > 40 --> "+cursor.count()); */
		System.out.println("Company Count  --> " + cursor.count());
		System.out.println("Record: " + cursor.toString());
		System.out.println("Record: " + cursor.next());

		mongoClient.close();

	}

	/*
	 * @SuppressWarnings("deprecation") public static void main(String[] args)
	 * throws UnknownHostException { MongoClient mongoClient = new
	 * MongoClient("10.34.180.56", 27017);
	 * 
	 * DB dbName = mongoClient.getDB("CompanyDB");
	 * 
	 * DBCollection collection = dbName.getCollection("company");
	 * 
	 * System.out.println("- Database: " + dbName);
	 * System.out.println("\t + Collection: " + collection);
	 * 
	 * DBCursor cursor = collection.find(); try { while(cursor.hasNext()) {
	 * System.out.println(cursor.next()); } } finally { cursor.close(); }
	 * 
	 * 
	 * BasicDBObject query = new BasicDBObject("age", new BasicDBObject("$gt",
	 * 40)); BasicDBObject query = new BasicDBObject("name", "ABC");
	 * BasicDBObject query2 = new BasicDBObject("$natural", -1);
	 * 
	 * DBObject myDoc = collection.findOne(query);
	 * System.out.println("Highest scoring document: "+ myDoc);
	 * 
	 * cursor = collection.find(query).sort(query2).limit(1);
	 * System.out.println("Person with age > 40 --> "+cursor.count());
	 * System.out.println("Company Count  --> "+cursor.count());
	 * System.out.println("Record: "+cursor.toString());
	 * System.out.println("Record: "+cursor.next());
	 * 
	 * mongoClient.close();
	 * 
	 * }
	 */

	/*
	 * @SuppressWarnings("deprecation") public static void main(String[] args)
	 * throws UnknownHostException { MongoClient mongoClient = new
	 * MongoClient("10.34.180.56", 27017);
	 * 
	 * List<String> databases = mongoClient.getDatabaseNames();
	 * 
	 * for (String dbName : databases) { System.out.println("- Database: " +
	 * dbName);
	 * 
	 * DB db = mongoClient.getDB(dbName);
	 * 
	 * Set<String> collections = db.getCollectionNames(); for (String colName :
	 * collections) { System.out.println("\t + Collection: " + colName); } }
	 * 
	 * mongoClient.close();
	 * 
	 * }
	 */

}
