package com.abc.temp.sqats.automation.page.objects;

import org.openqa.selenium.WebDriver;

/**
 * Class is an abstract Page class which provides methods to define a Page
 * 
 * @author hmalick
 *
 */
public abstract class Page {

	protected WebDriver driver;

	/**
	 * Method is used to set the driver for Selenium Webdriver. Provide the
	 * driver for Selenium Webdriver
	 * 
	 * @param driver
	 */
	public Page(WebDriver driver) {
		this.driver = driver;
	}

}
