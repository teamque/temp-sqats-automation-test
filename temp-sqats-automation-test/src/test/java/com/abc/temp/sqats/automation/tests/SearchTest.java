package com.abc.temp.sqats.automation.tests;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.abc.temp.sqats.automation.page.objects.SearchPage;
import com.abc.temp.sqats.automation.util.BaseTestUtil;
import com.abc.temp.sqats.automation.util.FileHandlerUtil;
import com.abc.temp.sqats.automation.util.ParameterDataSetup;
import com.abc.temp.sqats.automation.util.TestMetaData;
import com.jayway.restassured.path.json.JsonPath;

/**
 * Class utilizes the page objects, functions and utility classes to run TestNG
 * Test/s using Selenium Webdriver
 * 
 * @author hmalick
 *
 */
public class SearchTest extends BaseTestUtil {

	private static final Logger log = LoggerFactory.getLogger(SearchTest.class);
	private final static String Test_Name = "SearchTest";

	JsonPath testData = null;

	SearchPage searchPage;

	/**
	 * Method helps to setup the browser before the Test/s starts and gets the
	 * test data needed for the Test/s. Provide the testbrowser such as chrome,
	 * firefox etc
	 * 
	 * @param testbrowser
	 * @throws Exception
	 */
	@Parameters({ "testbrowser" })
	@BeforeTest
	public void setup(String testbrowser) throws Exception {
		log.info("Starting Test Case: " + Test_Name);

		testData = FileHandlerUtil
				.getFileJsonPathFromTestResource(ParameterDataSetup.testDataFilePath + "search/search.json");

	}

	/**
	 * Method helps tear down the browser after the Test/s are complete
	 */
	@AfterTest
	public void teardown() {
		try {
			log.info("Finished Test Case: " + Test_Name);
			driver.quit();

		} catch (Exception ex) {
			log.error("Failed", ex);
		}
	}

	/**
	 * Sample Test that utilizes the page object, functions and utilities to run
	 * a Test. Test also has the priority and testcaseId from Testlink
	 */
	@Test(priority = 0)
	@TestMetaData(testcase = "SQATS-1")
	public void ValidateGoogleSearchPageLoadsOnBrowserTest() {
		try {
			searchPage = new SearchPage(driver);

			searchPage.searchUrl();

		} catch (NoSuchElementException e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		} catch (Exception e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		}
	}

	@Test(priority = 1)
	@TestMetaData(testcase = "SQATS-2")
	public void ValidateCSUFCanBeSearchedOnGoogleTest() {
		try {
			searchPage.searchGoogle(testData);

		} catch (NoSuchElementException e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		} catch (Exception e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		}
	}

	@Test(priority = 2)
	@TestMetaData(testcase = "SQATS-3")
	public void ValidateCSUFResultsAreDisplayedOnGoogleResultsTest() {
		try {
			searchPage.validateSearchResult(testData);
		} catch (NoSuchElementException e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		} catch (Exception e) {
			Assert.fail("Fail: " + e.getMessage(), e);
		}
	}

}
