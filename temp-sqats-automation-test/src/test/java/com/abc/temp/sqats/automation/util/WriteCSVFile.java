/*
------------------------
	Common Functions
	This file contains common functions that have been identified which can be used in certain Test Cases for automation
	New functions could be included depending on the project requirement. 
	Each function could be called in the Test Case using the Classname.Function (ex: CommonFunctions.fScreenShot)
	Date: 02/07/2012
	Author: Harith Malick
	Application  = KingFisher
------------------------
*/

package com.abc.temp.sqats.automation.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import net.sourceforge.htmlunit.corejs.javascript.ast.ErrorCollector;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




/**
 * Class provides methods to write to a csv file
 * 
 * @author hmalick
 *
 */
public class WriteCSVFile {
	
		
	//ErrorCollector is used to capture the Throwable error in the script
	//@Rule
	public static ErrorCollector errorCollector = new ErrorCollector();
	private static final Logger log = LoggerFactory
			.getLogger(WriteCSVFile.class);
	
/*	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Function Name:	fObjectExists
		Function Description: To Check if an object exists
		Variable: 	byObjectName = Element used to identify the object either using xpath, element id etc
					intWaitCount = holds the number of seconds to wait
					webObjectDriver = Defined web driver either Firefox or IE
		Example: CommonFunctions.fObjectExists(driver, 60, By.id("btnNext"));
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/	
	/**
	 * Method is used to Check if an object exists. Provide the following parameters
	 * webObjectDriver = Defined web driver either Firefox or IE
	 * intWaitCount = holds the number of seconds to wait
	 * byObjectName = Element used to identify the object either using xpath, element id etc
	 * 			
	 * @param webObjectDriver
	 * @param intWaitCount
	 * @param byObjectName
	 */
	public static void fObjectExists(WebDriver webObjectDriver, int intWaitCount, By byObjectName){
		
		for (int second = 0;; second++) {
			if (second >= intWaitCount) //fail("timeout");
			try { if (isElementPresent(byObjectName,webObjectDriver)) 
				//return true;
				break;}
			catch (Exception e) {
				//return false;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static boolean isElementPresent(By by, WebDriver Driver) {
		try {
			Driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
	
	
/*	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Function Name:	fButtonClick
		Function Description: To Click a button
		Variable: 	byObjectName = Element used to identify the object either using xpath, element id etc
					webObjectDriver = Defined web driver either Firefox or IE
		Example: CommonFunctions.fButtonClick(driver, By.id("btnFltSearch"));
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/
	/**
	 * Method is used to click a button. Provide the following parameters
	 * webObjectDriver = Defined web driver either Firefox or IE
	 * byObjectName = Element used to identify the object either using xpath, element id etc
	 * 
	 * @param webObjectDriver
	 * @param byObjectName
	 */
	public static void fButtonClick(WebDriver webObjectDriver, By byObjectName){
		
		try{
			webObjectDriver.findElement(byObjectName).click();
		}catch (Throwable t){
			
			//errorCollector.addError(t);
			
		}
		
		
	}
	
/*	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Function Name:	fScreenShotError
		Function Description:  Takes a Screen Shot of an Error
		Variable: 	strTablePathS = Path to which the screen shot needs to be saved
					webObjectDriver = Defined web driver either Firefox or IE
		Example: CommonFunctions.fScreenShotError(1,driver,l_strTablePathS);
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/
	/**
	 * Method is used to take a screenshot of an error. Provide the following parameters
	 * webObjectDriver = Defined web driver either Firefox or IE
	 * strTablePathS = Path to which the screen shot needs to be saved
	 * 
	 * @param webObjectDriver
	 * @param strTablePathS
	 */
	public static void fScreenShotError(WebDriver webObjectDriver, String strTablePathS){
		
		String filename=strTablePathS+"ScreenShot-"+getDateAndHour()+".jpg";
		File scrFile = ((TakesScreenshot)webObjectDriver).getScreenshotAs(OutputType.FILE);
		System.out.println("Your screen shot has been generated!");
				
		try {
			FileUtils.copyFile(scrFile, new File(filename));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Timestamp function		
	public static String getDateAndHour() {

		String today;

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
		Calendar calendar = Calendar.getInstance();

		today = dateFormat.format(calendar.getTime());

		return today;

		} 
	
	public static String filenameR="Results-"+getDateAndHour()+".xls";
	public static String filenameR2="ReportTemplate2.xls";
	
/*	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		Function Name:	fCreateExcelResultsFile
		Function Description:  This function creates a results file
		Variable: 	strTablePathR = Path to which the result excel is created
		Example: CommonFunctions.fCreateExcelResultsFile(l_strTablePathR);
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/
	/**
	 * Method is used to create a results file. Provide the following parameters
	 * strTablePathR = Path to which the result excel is created
	 * 
	 * @param strTablePathR
	 */
	public static void fCreateExcelResultsFile(String strTablePathR){
		try{
		
		String filename=strTablePathR+filenameR;		
		HSSFWorkbook hwb=new HSSFWorkbook();
		HSSFSheet sheet =  hwb.createSheet("Results");
		HSSFSheet sheet2 =  hwb.createSheet("Overall Report");

		HSSFRow rowhead=   sheet.createRow((int)0);
		rowhead.createCell((int) 0).setCellValue("Test_Scenario_Description");
		rowhead.createCell((int) 1).setCellValue("Expected_Result");
		rowhead.createCell((int) 2).setCellValue("Actual_Result");
		rowhead.createCell((int) 3).setCellValue("Test_Status");

		FileOutputStream fileOut =  new FileOutputStream(filename);
		hwb.write(fileOut);
		fileOut.close();
		System.out.println("Your excel file has been generated!");

		} catch ( Exception ex ) {
		    System.out.println(ex);

		}
		 
	}
	
	
	public static void fGenerateReport(String strTablePathR){
		try{
		
			String filename=strTablePathR+filenameR;
			String filename2=strTablePathR+filenameR2;
			FileInputStream fileIn = new FileInputStream(filename);
			HSSFWorkbook wb = new HSSFWorkbook(fileIn);
			Sheet sheet =  wb.getSheetAt(1);
			
			Row row =   sheet.createRow((int)0);
			row.createCell((int) 0).setCellValue("TCs Passed");
			row.createCell((int) 1).setCellValue("0");
			String StrFormula = "COUNTIF(Results!D2:D200, \"Pass\")";
			row.createCell((int) 1).setCellType(HSSFCell.CELL_TYPE_FORMULA);
			row.createCell((int) 1).setCellFormula(StrFormula);
			
			Row row2 = sheet.createRow((int)1);
			row2.createCell((int) 0).setCellValue("TCs Failed");
			row2.createCell((int) 1).setCellValue("0");
			String StrFormula2 = "COUNTIF(Results!D2:D200, \"Fail\")";
			row2.createCell((int) 1).setCellType(HSSFCell.CELL_TYPE_FORMULA);
			row2.createCell((int) 1).setCellFormula(StrFormula2);
			
			Row row3 = sheet.createRow((int)2);
			row3.createCell((int) 0).setCellValue("Total TCs");
			row3.createCell((int) 1).setCellValue("0");
			String StrFormula3 = "SUM(B1:B2)";
			row3.createCell((int) 1).setCellType(HSSFCell.CELL_TYPE_FORMULA);
			row3.createCell((int) 1).setCellFormula(StrFormula3);
			
						
			FileOutputStream fileOut =  new FileOutputStream(filename);
			wb.write(fileOut);
			fileOut.close();
			System.out.println("Excel file generated!");

		} catch ( Exception ex ) {
		    System.out.println(ex);

		}
		 
	}
	
/*	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Function Name:	fReportResult
	Function Description:  This function creates a results file
	Variable: 	strTablePathR = Path at which the result excel is created
				l_strTSDescription = Description of the test scenario
				l_strExpectedResult = Description of the expected result
				l_strActualResult = Description of the actual result
				l_strStatus = Status of the test scenario
	Example: CommonFunctions.fReportResult(1,l_strTablePathR,l_strTSDescription,"Successfully Load the Widget Page.","Successfully Load the Widget Page.","Pass");
	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/	
	/**
	 * Method is used to creates a results file. Provide the following parameters
	 * l_strTSDescription = Description of the test scenario
	 * l_strExpectedResult = Description of the expected result
	 * l_strActualResult = Description of the actual result
	 * l_strStatus = Status of the test scenario
	 * 
	 * @param strTablePathR
	 * @param l_strTSDescription
	 * @param l_strExpectedResult
	 * @param l_strActualResult
	 * @param l_strStatus
	 * @param l_strCount
	 */
	public static void fReportResult(String strTablePathR, String l_strTSDescription, String l_strExpectedResult, String l_strActualResult, String l_strStatus, int l_strCount){
		try{
		/*String filename=strTablePathR+"Results-"+getDateAndHour()+".xls";*/		 
		String filename=strTablePathR+filenameR;
		String filename2=strTablePathR+filenameR2;	
		if (l_strCount == 1){
			FileInputStream fileIn = new FileInputStream(filename2);
			HSSFWorkbook wb = new HSSFWorkbook(fileIn);
			Sheet sheet =  wb.getSheetAt(0);
			
			int numberofRows = firstEmptyRowPosition(sheet);
			Row row =   sheet.createRow((int)numberofRows);
			row.createCell((int) 0).setCellValue(l_strTSDescription);
			row.createCell((int) 1).setCellValue(l_strExpectedResult);
			row.createCell((int) 2).setCellValue(l_strActualResult);
			row.createCell((int) 3).setCellValue(l_strStatus);
						
	
			FileOutputStream fileOut =  new FileOutputStream(filename);
			wb.write(fileOut);
			fileOut.close();
			System.out.println("Excel file updated!");
			
		}else{
			FileInputStream fileIn = new FileInputStream(filename);
			HSSFWorkbook wb = new HSSFWorkbook(fileIn);
			Sheet sheet =  wb.getSheetAt(0);
			
			int numberofRows = firstEmptyRowPosition(sheet);
			Row row =   sheet.createRow((int)numberofRows);
			row.createCell((int) 0).setCellValue(l_strTSDescription);
			row.createCell((int) 1).setCellValue(l_strExpectedResult);
			row.createCell((int) 2).setCellValue(l_strActualResult);
			row.createCell((int) 3).setCellValue(l_strStatus);
						
	
			FileOutputStream fileOut =  new FileOutputStream(filename);
			wb.write(fileOut);
			fileOut.close();
			System.out.println("Excel file updated!");
		}							
		
		
		}catch ( Exception ex ) {
	    System.out.println(ex);

		}
	}
	
	public static int firstEmptyRowPosition(final Sheet sheet) {
        int columnCount = 0;
        for (Row row : sheet) {
        	Cell cell=row.getCell(1);
            if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                break;
            }
            columnCount++;
        }
        return columnCount;
    }
		
	public void fUpdateCSVFile(String strTablePathR){
		try{
		
		String filename=strTablePathR;	
		FileInputStream fis = new FileInputStream(filename);
		HSSFWorkbook hwb=new HSSFWorkbook(fis);
		String sheetName =  hwb.getSheetName(0); //("Request_USAA");
		HSSFSheet sheet = hwb.getSheet(sheetName);
		/*Sheet sheet =  hwb.getSheetAt(1);*/
		log.info("Open CSV file for update");
		
		Row rowhead = sheet.getRow(2);
		if (isEmpty(rowhead)) {
			log.error("Invalid row");
        } else {
			rowhead.getCell((int) 21).setCellValue("");
			rowhead.getCell((int) 21).setCellValue("111111120");
			log.info("Set file SSN value");
        }
		
		FileOutputStream fileOut =  new FileOutputStream(filename);
		hwb.write(fileOut);
		fileOut.close();
		log.info("Excel file has been updated!");

		} catch (Exception e) {
			log.info("Set file SSN value");
			log.error("Fail", e);
		}
		 
	}
	
	private boolean isEmpty(final Row row) {
        Cell firstCell = row.getCell(0);
        boolean rowIsEmpty = (firstCell == null)
                || (firstCell.getCellType() == Cell.CELL_TYPE_BLANK);
        return rowIsEmpty;
    }

}
