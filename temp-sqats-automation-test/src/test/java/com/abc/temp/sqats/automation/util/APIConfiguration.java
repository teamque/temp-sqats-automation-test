package com.abc.temp.sqats.automation.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

/**
 * Class is a base util class used to configure URIs for RESTful API Test/s
 * 
 * @author hmalick
 *
 */
public class APIConfiguration {

	private static final Logger log = LoggerFactory.getLogger(APIConfiguration.class);

	public static String environment;
	public static boolean proxyTest;

	public static MicConfiguration micProxy;
	public static MicConfiguration micSecurity;
	public static MicConfiguration micAccount;
	public static MicConfiguration micCompany;
	public static MicConfiguration micCandidate;
	public static MicConfiguration micPackage;
	public static MicConfiguration micOrder;
	public static MicConfiguration micReport;
	public static MicConfiguration micSupport;
	public static MicConfiguration micUser;
	public static MicConfiguration micWebhook;
	public static MicConfiguration micStatusChange;
	public static MicConfiguration micFulfillment;
	public static MicConfiguration micWorkflow;
	public static MicConfiguration micWorkflowSession;
	public static MicConfiguration micWorkflowSessionExecutor;
	public static MicConfiguration micDocumentDeliveryRequest;
	public static MicConfiguration micDocumentDeliveryAttempt;
	public static MicConfiguration micDocumentDeliveryVendorConfig;
	public static MicConfiguration micDocumentNotificationRequest;
	public static MicConfiguration micTemplateType;
	public static MicConfiguration micTemplate;
	public static MicConfiguration micTemplateRule;
	public static MicConfiguration micTemplateGroup;
	public static MicConfiguration micDocumentNotificationConfig;
	public static MicConfiguration micDocument;
	public static MicConfiguration micApiv2;
	public static MicConfiguration micphxCompany;
	public static MicConfiguration micphxSubject;
	public static MicConfiguration micphxPackage;
	public static MicConfiguration micphxOrder;
	public static MicConfiguration micphxUser;
	public static MicConfiguration micphxVendor;
	public static MicConfiguration micphxSearch;
	public static MicConfiguration micphxLegacyCompany;
	public static MicConfiguration micphxLegacyCompanyConfig;
	public static MicConfiguration micphxLegacyUser;
	public static MicConfiguration micphxOrderIntegration;
	public static MicConfiguration micphxEducationDB;
	public static MicConfiguration micphxLegacyPublisher;
	public static MicConfiguration micphxClientMigration;
	public static MicConfiguration micphxSearchInitiator;
	public static MicConfiguration micphxMvrFulfillment;
	public static MicConfiguration micphxMvrAutoAdj;
	public static MicConfiguration micphxCamundaWorkflow;
	public static MicConfiguration micphxFedCrim;
	public static MicConfiguration micphxFM;
	public static MicConfiguration micphxSearchToLegacy;

	/**
	 * Method is used to setup the configuration for RESTful API Test/s
	 * 
	 * @param context
	 */
	@BeforeTest
	@DataProvider(name = "DataProvider")
	public static void setupData(ITestContext context) {
		try {
			APIConfiguration.environment = getString(context, "environment");
			String systemPropEnvironment = System.getProperty("environment");
			if (null != systemPropEnvironment) {
				log.info("#### Overriding environment property with value from system.property: "
						+ systemPropEnvironment);
				APIConfiguration.environment = systemPropEnvironment;
			}
			APIConfiguration.proxyTest = getBoolean(context, "proxyTest");

			String prefix = APIConfiguration.proxyTest ? "proxy" : "mic";

			micProxy = setUpMicConfiguration(context, null, "Proxy");
			micCompany = setUpMicConfiguration(context, prefix, "Company");
			micSecurity = setUpMicConfiguration(context, prefix, "Security");
			micAccount = setUpMicConfiguration(context, prefix, "Account");
			micCandidate = setUpMicConfiguration(context, prefix, "Candidate");
			micPackage = setUpMicConfiguration(context, prefix, "Package");
			micOrder = setUpMicConfiguration(context, prefix, "Order");
			micReport = setUpMicConfiguration(context, prefix, "Report");
			micSupport = setUpMicConfiguration(context, prefix, "Support");
			micUser = setUpMicConfiguration(context, prefix, "User");
			micWebhook = setUpMicConfiguration(context, prefix, "Webhook");

			micStatusChange = setUpMicConfiguration(context, prefix, "StatusChange");
			micFulfillment = setUpMicConfiguration(context, prefix, "Fulfillment");
			micWorkflow = setUpMicConfiguration(context, prefix, "Workflow");
			micWorkflowSession = setUpMicConfiguration(context, prefix, "WorkflowSession");
			micWorkflowSessionExecutor = setUpMicConfiguration(context, prefix, "WorkflowSessionExecutor");
			micDocumentDeliveryRequest = setUpMicConfiguration(context, prefix, "DocumentDeliveryRequest");
			micDocumentDeliveryAttempt = setUpMicConfiguration(context, prefix, "DocumentDeliveryAttempt");
			micDocumentDeliveryVendorConfig = setUpMicConfiguration(context, prefix, "DocumentDeliveryVendorConfig");
			micDocumentNotificationRequest = setUpMicConfiguration(context, prefix, "DocumentNotificationRequest");
			micTemplateType = setUpMicConfiguration(context, prefix, "TemplateType");
			micTemplate = setUpMicConfiguration(context, prefix, "Template");
			micTemplateRule = setUpMicConfiguration(context, prefix, "TemplateRule");
			micTemplateGroup = setUpMicConfiguration(context, prefix, "TemplateGroup");
			micDocumentNotificationConfig = setUpMicConfiguration(context, prefix, "DocumentNotificationConfig");
			micDocument = setUpMicConfiguration(context, prefix, "Document");
			micApiv2 = setUpMicConfiguration(context, prefix, "Apiv2");

			micphxCompany = setUpMicConfiguration(context, prefix, "phxCompany");
			micphxSubject = setUpMicConfiguration(context, prefix, "phxSubject");
			micphxPackage = setUpMicConfiguration(context, prefix, "phxPackage");
			micphxOrder = setUpMicConfiguration(context, prefix, "phxOrder");
			micphxUser = setUpMicConfiguration(context, prefix, "phxUser");
			micphxVendor = setUpMicConfiguration(context, prefix, "phxVendor");
			micphxSearch = setUpMicConfiguration(context, prefix, "phxSearch");
			micphxLegacyCompany = setUpMicConfiguration(context, prefix, "phxLegacyCompany");
			micphxLegacyCompanyConfig = setUpMicConfiguration(context, prefix, "phxLegacyCompanyConfig");
			micphxLegacyUser = setUpMicConfiguration(context, prefix, "phxLegacyUser");
			micphxOrderIntegration = setUpMicConfiguration(context, prefix, "phxOrderIntegration");
			micphxEducationDB = setUpMicConfiguration(context, prefix, "phxEducationDB");
			micphxLegacyPublisher = setUpMicConfiguration(context, prefix, "phxLegacyPublisher");
			micphxClientMigration = setUpMicConfiguration(context, prefix, "phxClientMigration");
			micphxSearchInitiator = setUpMicConfiguration(context, prefix, "phxSearchInitiator");
			micphxMvrFulfillment = setUpMicConfiguration(context, prefix, "phxMvrFulfillment");
			micphxMvrAutoAdj = setUpMicConfiguration(context, prefix, "phxMvrAutoAdj");
			micphxCamundaWorkflow = setUpMicConfiguration(context, prefix, "phxCamundaWorkflow");
			micphxFedCrim = setUpMicConfiguration(context, prefix, "phxFedCrim");
			micphxFM = setUpMicConfiguration(context, prefix, "phxFM");
			micphxSearchToLegacy = setUpMicConfiguration(context, prefix, "phxSearchToLegacy");

			System.out.println();
			System.out.println("DM API configurations");
			System.out.println("================================================================================");
			System.out.println(" environment  : " + environment);
			System.out.println(" proxyTest    : " + proxyTest);
			System.out.println(" micProxy     : " + micProxy);
			System.out.println(" micCompany   : " + micCompany);
			System.out.println(" micSecurity  : " + micSecurity);
			System.out.println(" micAccount   : " + micAccount);
			System.out.println(" micCandidate : " + micCandidate);
			System.out.println(" micPackage   : " + micPackage);
			System.out.println(" micOrder     : " + micOrder);
			System.out.println(" micReport     : " + micReport);
			System.out.println(" micSupport     : " + micSupport);
			System.out.println(" micUser     : " + micUser);
			System.out.println(" micWebhook     : " + micWebhook);
			System.out.println(" micStatusChange    : " + micStatusChange);
			System.out.println(" micFulfillment     : " + micFulfillment);
			System.out.println(" micWorkflow     : " + micWorkflow);
			System.out.println(" micWorkflowSession     : " + micWorkflowSession);
			System.out.println(" micWorkflowSessionExecutor     : " + micWorkflowSessionExecutor);
			System.out.println(" micDocumentDeliveryRequest " + micDocumentDeliveryRequest);
			System.out.println(" micDocumentDeliveryAttempt " + micDocumentDeliveryAttempt);
			System.out.println(" micDocumentNotificationRequest " + micDocumentNotificationRequest);
			System.out.println(" micTemplateType " + micTemplateType);
			System.out.println(" micTemplate " + micTemplate);
			System.out.println(" micTemplateRule " + micTemplateRule);
			System.out.println(" micTemplateGroup " + micTemplateGroup);
			System.out.println(" micDocumentNotificationConfig" + micDocumentNotificationConfig);
			System.out.println(" micDocument" + micDocument);
			System.out.println(" micApiv2" + micApiv2);
			System.out.println(" micDocumentDeliveryVendorConfig " + micDocumentDeliveryVendorConfig);
			System.out.println(" micphxCompany " + micphxCompany);
			System.out.println(" micphxSubject " + micphxSubject);
			System.out.println(" micphxPackage " + micphxPackage);
			System.out.println(" micphxOrder " + micphxOrder);
			System.out.println(" micphxUser " + micphxUser);
			System.out.println(" micphxVendor " + micphxVendor);
			System.out.println(" micphxSearch " + micphxSearch);
			System.out.println(" micphxLegacyCompany " + micphxLegacyCompany);
			System.out.println(" micphxLegacyCompanyConfig " + micphxLegacyCompanyConfig);
			System.out.println(" micphxLegacyUser " + micphxLegacyUser);
			System.out.println(" micphxOrderIntegration " + micphxOrderIntegration);
			System.out.println(" micphxEducationDB " + micphxEducationDB);
			System.out.println(" micphxLegacyPublisher " + micphxLegacyPublisher);
			System.out.println(" micphxClientMigration " + micphxClientMigration);
			System.out.println(" micphxSearchInitiator " + micphxSearchInitiator);
			System.out.println(" micphxMvrFulfillment " + micphxMvrFulfillment);
			System.out.println(" micphxMvrAutoAdj " + micphxMvrAutoAdj);
			System.out.println(" micphxCamundaWorkflow " + micphxCamundaWorkflow);
			System.out.println(" micphxFedCrim " + micphxFedCrim);
			System.out.println(" micphxFM " + micphxFM);
			System.out.println(" micphxSearchToLegacy " + micphxSearchToLegacy);
			System.out.println("================================================================================");
			System.out.println();

		} catch (Exception e) {
			log.error("Fail", e);
			Assert.fail("Fail to setup APIConfiguration", e);
		}

	}

	/**
	 * Method is used to setup micro service configurations for RESTful API
	 * Test/s
	 * 
	 * @param context
	 * @param prefix
	 * @param service
	 * @return
	 * @throws Exception
	 */
	public static MicConfiguration setUpMicConfiguration(ITestContext context, String prefix, String service)
			throws Exception {
		log.info("Preparing config for: prefix[" + prefix + "] service[" + service + "]");
		String domain = getString(context, prefix, service + "Domain");
		Integer port = getInteger(context, prefix, service + "Port");
		String baseUrl = getString(context, prefix, service + "BaseUrl");
		MicConfiguration config = null;
		if (null != domain && null != baseUrl) {
			config = new MicConfiguration();
			config.setMicDomain(getString(context, prefix, service + "Domain"));
			config.setMicPort(getInteger(context, prefix, service + "Port"));
			config.setMicBaseUrl(getString(context, prefix, service + "BaseUrl"));
		}
		return config;
	}

	/**
	 * Method to get key for micro service configuration for RESTful API Test/s
	 * 
	 * @param prefix
	 * @param environment
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static String getKey(String prefix, String environment, String key) throws Exception {
		StringBuilder newKey = new StringBuilder();
		if (null != prefix) {
			newKey.append(prefix);
		}
		newKey.append(key);
		if (null != environment) {
			newKey.append("_" + environment);
		}
		return newKey.toString();
	}

	/**
	 * Method to get string for micro service configuration for RESTful API
	 * Test/s
	 * 
	 * @param context
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static String getString(ITestContext context, String key) throws Exception {
		return getString(context, null, key);
	}

	public static String getString(ITestContext context, String prefix, String key) throws Exception {
		String tmpKey = getKey(prefix, environment, key);
		String value = context.getCurrentXmlTest().getParameter(tmpKey);
		if (null == value) {
			// if env variable not exist, then look for default w/o env
			tmpKey = getKey(prefix, null, key);
			value = context.getCurrentXmlTest().getParameter(tmpKey);
		}
		return value;
	}

	/**
	 * Method to get integer for micro service configuration for RESTful API
	 * Test/s
	 * 
	 * @param context
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static Integer getInteger(ITestContext context, String key) throws Exception {
		return Integer.valueOf(getString(context, null, key));
	}

	public static Integer getInteger(ITestContext context, String prefix, String key) throws Exception {
		String tmp = getString(context, prefix, key);
		return null != tmp ? Integer.valueOf(tmp) : null;
	}

	/**
	 * Method to get boolean value for micro service configuration for RESTful
	 * API Test/s
	 * 
	 * @param context
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static Boolean getBoolean(ITestContext context, String key) throws Exception {
		return Boolean.valueOf(getString(context, null, key));
	}

	public static Boolean getBoolean(ITestContext context, String prefix, String key) throws Exception {
		return Boolean.valueOf(getString(context, prefix, key));
	}

}
