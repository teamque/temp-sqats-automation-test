package com.abc.temp.sqats.automation.util;

/**
 * Class used to manage enums for the micro service configuration for RESTful API Test/s
 * 
 * @author hmalick
 *
 */
public enum APIMiceroservicesEnum {

	PROXY("mic-api-proxy"),
	SECURITY("mic-api-security"),
	ACCOUNT("mic-api-account"),
	COMPANY("mic-api-company"),
	CANDIDATE("mic-api-candidate"),
	PACKAGE("mic-api-package"),
	ORDER("mic-api-order"),
	REPORT("mic-api-report"),
	FULFILLMENT("mic-api-fulfillment"),
	STATUS_CHANGE("mic-abc-status-change-publisher"),
	;
	
	private String name;
	private APIMiceroservicesEnum(String name){
		this.setName(name);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
