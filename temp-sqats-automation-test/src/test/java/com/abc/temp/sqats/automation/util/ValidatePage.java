package com.abc.temp.sqats.automation.util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

/**
 * Class provides methods to validate the test/s output
 * 
 * @author hmalick
 *
 */
public class ValidatePage extends BaseUtil {

	private static final Logger log = LoggerFactory
			.getLogger(ValidatePage.class);

	/**
	 * Method is used just to Validate a page by searching for specific content on
	 * the page then logging a message depending on if its found or unable to
	 * find
	 * 
	 * @param driver
	 *            Always Required
	 * @param Page_Content
	 *            Specific content on that page
	 * @param Print_on_Pass
	 *            What to log if found
	 * @param Print_on_Fail
	 *            What to log if NOT found
	 */
	public void validatePage(WebDriver driver, String Page_Content,
			String Print_on_Pass, String Print_on_Fail) {

		try {
			Boolean textFound = true;
			String delimiter = "[ ]+";
			String[] textArray = Page_Content.split(delimiter);

			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			// Is the words within the Link_Text are available on the page? If
			// so, set textFound to true
			for (int i = 0; i < textArray.length; i++)
				if (driver.getPageSource().contains(textArray[i])) {
					textFound = true;
				} else {
					textFound = false;
					break;
				}

			if (textFound) {
				log.info(Print_on_Pass);
			} else {
				log.info(Print_on_Fail);
				log.error("Unable to locate the test ==> \"" + Page_Content
						+ "\" in the page with the tile "
						+ driver.getTitle().toString() + " and URL: "
						+ driver.getCurrentUrl() + "\n");
				Assert.fail();
			}
		} catch (Exception e) {
			log.info(Print_on_Fail);
			log.error("Unable to locate the " + Page_Content + ".\n");
			log.error("Fail ", e);
			TakeScreenSnapshot file = new TakeScreenSnapshot();
			file.GetScreenSnapshot(driver, null);
			Assert.fail("Unable to locate the " + Page_Content + ".", e);
		}

	}
	
	/**
	 * Method is used just to Validate a page by searching for specific content on
	 * the page then logging a message depending on if its found or unable to
	 * find
	 * 
	 * @param driver
	 * @param Page_Content
	 * @param Page_Content_Alternate
	 * @param Print_on_Pass
	 * @param Print_on_Fail
	 */
	public void validatePageAKA(WebDriver driver, String Page_Content, String Page_Content_Alternate,
			String Print_on_Pass, String Print_on_Fail) {

		try {
			Boolean textFound = true;
			String delimiter = "[ ]+";
			String[] textArray = Page_Content.split(delimiter);

			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			// Is the words within the Link_Text are available on the page? If
			// so, set textFound to true
			for (int i = 0; i < textArray.length; i++)
				if (driver.getPageSource().contains(textArray[i])) {
					textFound = true;
				} else {
					textFound = false;
					break;
				}

			if (textFound) {
				log.info(Print_on_Pass);
			} else {
				log.info(Print_on_Fail);
				validatePage(driver, Page_Content_Alternate,
						"AKA Selection Process records displayed.",
						"AKA Selection Process records are not displayed.");
			}
		} catch (Exception e) {
			log.info(Print_on_Fail);
			log.error("Unable to locate the " + Page_Content + ".\n");
			log.error("Fail ", e);
			Assert.fail();
		}
	}
}