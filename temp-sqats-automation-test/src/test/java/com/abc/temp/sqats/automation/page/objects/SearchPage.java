package com.abc.temp.sqats.automation.page.objects;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.abc.temp.sqats.automation.util.GetURL;
import com.abc.temp.sqats.automation.util.ParameterDataSetup;
import com.abc.temp.sqats.automation.util.SetBrowser;
import com.jayway.restassured.path.json.JsonPath;

/**
 * Class defines the objects and methods for the SearchPage and extends the
 * abstract Page class
 * 
 * @author hmalick
 *
 */
public class SearchPage extends Page {

	private static final Logger log = LoggerFactory.getLogger(SearchPage.class);

	public SearchPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}

	/**
	 * Method defines the Search Field object
	 */
	@FindBy(name = "q")
	public WebElement searchField;

	/**
	 * Method defines the Search Button object
	 */
	@FindBy(name = "btnK")
	public WebElement searchButton;

	/**
	 * Method loads the Search Url on a Webbrowser defined by the driver of
	 * Selenium Webdriver
	 * 
	 * @throws MalformedURLException
	 */
	public void searchUrl() throws MalformedURLException {

		SetBrowser set = new SetBrowser();
		if (ParameterDataSetup.testSetBrowser.equals("yes")) {

		} else {
			driver = set.setBrowser(ParameterDataSetup.testBrowser);
		}
		GetURL get = new GetURL();
		get.setSearchURL(driver, ParameterDataSetup.testEnvironment);
		driver.manage().window().maximize();

	}

	/**
	 * Method performs a Google Search based on the data that has been provided.
	 * Provide the data to search
	 * 
	 * @param data
	 */
	public void searchGoogle(JsonPath data) {
		try {

			log.info("Search Google");

			log.info("Search Text: " + data.getString("search.text"));
			searchField.sendKeys(data.getString("search.text"));

			log.info("Click Search");
			searchButton.click();

		} catch (Exception e) {
			log.error("Unable to perform search");
			log.error("Fail ", e);
			Assert.fail("Fail ", e);
		}
	}
	
	/**
	 * Method performs a validation of the Google Search.
	 * Provide the data to search
	 * 
	 * @param data
	 */
	public void validateSearchResult(JsonPath data) {
		try {

			log.info("Validate Search Result");

			log.info("Search Result: " + data.getString("search.result"));
			driver.getPageSource().contains(data.getString("search.result"));

		} catch (Exception e) {
			log.error("Unable to perform validation");
			log.error("Fail ", e);
			Assert.fail("Fail ", e);
		}
	}
}
