package com.abc.temp.sqats.automation.util;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.model.Build;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

/**
 * Class provides methods to perform functionalities with testlink
 * 
 * @author hmalick
 *
 */
public class TestlinkUtil {

	public static TestLinkAPI testlinkApi = null;

	/**
	 * Method used to execute test cases in testlink based on the passed or
	 * failed status of test/s. Provide test case id in testlink, status of the
	 * test and a comment
	 * 
	 * @param testlinkTestCase
	 * @param status
	 * @param comment
	 */
	public void executeTestlinkTest(String testlinkTestCase, String status, String comment) {

		System.out.println("Execute Testlink Test Case");
		System.out.println(
				"/******************************************************************************************************/");
		System.out.println("TestCaseTestlinkId: " + testlinkTestCase);

		String testProject = ParameterDataSetup.testTestlinkTestProject;
		String testPlan = ParameterDataSetup.testTestlinkTestPlan;
		String buildName = ParameterDataSetup.testEnvironment + "-Build";
		String notes = comment;
		ExecutionStatus result = null;
		Integer buildId = null;
		Integer testCaseId = null;
		String testCaseExternalId = testlinkTestCase;

		connectTestlink();

		if (status.equals("pass")) {
			result = ExecutionStatus.PASSED;
		} else if (status.equals("fail")) {
			result = ExecutionStatus.FAILED;
		}

		TestPlan testPlanName = testlinkApi.getTestPlanByName(testPlan, testProject);
		Integer testPlanId = testPlanName.getId();
		System.out.println("TestPlan: " + testPlanName);
		System.out.println("TestBuildName: " + buildName);

		Build[] testBuild = testlinkApi.getBuildsForTestPlan(testPlanId);

		for (int i = 0; i < testBuild.length; i++) {
			if (testBuild[i].getName().equals(buildName)) {
				buildId = testBuild[i].getId();
				break;
			}
		}
		System.out.println("TestBuildId: " + buildId);

		TestCase[] testCases = testlinkApi.getTestCasesForTestPlan(testPlanId, null, buildId, null, null, null, null,
				null, null, null, null);

		for (int i = 0; i < testCases.length; i++) {
			if (testCases[i].getFullExternalId().equals(testCaseExternalId)) {
				testCaseId = testCases[i].getId();
				break;
			}
		}
		System.out.println("TestCaseExternalId: " + testCases[0].getFullExternalId());
		System.out.println("TestCaseId: " + testCaseId);

		reportResult(testPlanId, testCaseId, buildId, notes, result);

		System.out.println(
				"/******************************************************************************************************/");
	}

	/**
	 * Method is used to connect to testlink
	 */
	public static void connectTestlink() {

		String devKey = ParameterDataSetup.testTestlinkDevKey;
		String url = ParameterDataSetup.testTestlinkUrl;
		URL testlinkURL = null;
		System.out.println("testlinkUrl: " + url);
		System.out.println("testlinkDevKey: " + devKey);

		try {
			testlinkURL = new URL(url);
		} catch (MalformedURLException mue) {
			mue.printStackTrace(System.err);
			System.exit(-1);
		}

		try {
			testlinkApi = new TestLinkAPI(testlinkURL, devKey);
		} catch (TestLinkAPIException te) {
			te.printStackTrace(System.err);
			System.exit(-1);
		}
	}

	public static void reportResult(Integer testPlanId, Integer testCaseId, Integer Build, String Notes,
			ExecutionStatus Result) {

		testlinkApi.reportTCResult(testCaseId, null, testPlanId, Result, Build, null, Notes, null, null, null, null,
				null, null);
	}

	// @Test(priority=0)
	public void Test0() throws Exception {

		String testProject = "Document Management";
		String testPlan = "Smoke Test";
		String buildName = "QA Build";
		String notes = "Test Comments";
		ExecutionStatus result = null;
		Integer buildId = null;
		Integer testCaseId = null;
		String testCaseExternalId = "DM-1";

		System.out.println("Testing Testlink Execution");

		connectTestlink();

		result = ExecutionStatus.PASSED;

		TestPlan testPlanName = testlinkApi.getTestPlanByName(testPlan, testProject);
		Integer testPlanId = testPlanName.getId();
		System.out.println("TestPlan: " + testPlanName);

		Build[] testBuild = testlinkApi.getBuildsForTestPlan(testPlanId);
		System.out.println("TestBuild: " + testBuild.toString());

		for (int i = 0; i < testBuild.length; i++) {
			if (testBuild[i].getName().equals(buildName)) {
				buildId = testBuild[i].getId();
				break;
			}
		}
		System.out.println("TestBuild: " + buildId);

		/* Integer buildId = testBuild[0].getId(); */

		TestCase[] testCases = testlinkApi.getTestCasesForTestPlan(testPlanId, null, buildId, null, null, null, null,
				null, null, null, null);
		System.out.println("TestCases: " + testCases.toString());

		for (int i = 0; i < testCases.length; i++) {
			if (testCases[i].getFullExternalId().equals(testCaseExternalId)) {
				testCaseId = testCases[i].getId();
				break;
			}
		}
		System.out.println("TestCaseExternalId: " + testCases[0].getFullExternalId());
		System.out.println("TestCaseId: " + testCaseId);

		reportResult(testPlanId, testCaseId, buildId, notes, result);

		System.out.println("Finished Testlink Execution");
	}

	/*
	 * @Test public void Test1()throws Exception{
	 * 
	 * TestlinkUpdateExample a=new TestlinkUpdateExample();
	 * 
	 * WebDriver driver=new FirefoxDriver(); WebDriverWait wait=new
	 * WebDriverWait(driver, 600);
	 * 
	 * String testProject="Gmail"; String testPlan="SampleTestPlan"; String
	 * testCase="GmailLogin1"; String build="SampleBuild"; String notes=null;
	 * String result=null;
	 * 
	 * try{
	 * 
	 * driver.manage().window().maximize(); driver.get(
	 * "https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/mail/&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1"
	 * ); driver.findElement(By.id("Email")).sendKeys("testlink.msoftgp");
	 * driver.findElement(By.id("Passwd")).sendKeys("*******");
	 * driver.findElement(By.id("signIn")).click();
	 * driver.switchTo().defaultContent();
	 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(
	 * "+Testlink")));
	 * 
	 * result= TestLinkAPIResults.TEST_PASSED; notes="Executed successfully";
	 * 
	 * } catch(Exception e){ result=TestLinkAPIResults.TEST_FAILED;
	 * notes="Execution failed"; } finally{ a.reportResult(testProject,
	 * testPlan, testCase, build, notes, result); driver.quit(); } }
	 */
}
