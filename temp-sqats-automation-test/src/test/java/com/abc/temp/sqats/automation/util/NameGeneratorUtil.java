package com.abc.temp.sqats.automation.util;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.text.WordUtils;

/**
 * Class provides methods to generate a random name
 * 
 * @author hmalick
 *
 */
public class NameGeneratorUtil {

	public static void main(String[] args) {
		try {
			/*
			 * for (int i = 0; i < 10; i++) { System.out.println(getRandom(0,
			 * 2)); System.out.println(generateName(true));
			 * System.out.println(); }
			 */

			System.out.println(WordUtils.capitalizeFully(
					"Validate candidateInfoComplete is false for Order object when order status is OIN (P5)",
					new char[] { ' ' }).replaceAll(" ", ""));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String generateName() {
		return generateName(false);
	}

	/**
	 * Method used to generate a random name that can be used for test data.
	 * Provide the show logs boolean value
	 * 
	 * @param showLogs
	 * @return
	 */
	public static String generateName(boolean showLogs) {

		StringBuilder logs = new StringBuilder();

		String[] keysArr = { "DAY", "MONTH", "COUNTRY" };
		List<String> keyss = Arrays.asList(keysArr);
		List<String> keys = new ArrayList<>(keyss);

		// System.out.println("keys: " + keys);
		logs.append("\n | keys: " + keys);

		Map<String, List<String>> dataListRaw = new HashMap<>();
		dataListRaw.put("DAY", getDaysOfWeek(false));
		dataListRaw.put("MONTH", getMonths(false));
		dataListRaw.put("COUNTRY", getCountries());

		// System.out.println("dataListRaw: " + dataListRaw);
		logs.append("\n | dataListRaw: " + dataListRaw);

		int removeListId = getRandom(0, 3);
		// System.out.println("removeListId: " + removeListId);
		String removeKey = keys.remove(removeListId);
		// System.out.println("removing key: " + removeListId + " | " +
		// removeKey);
		logs.append("\n | removing key: " + removeListId + " | " + removeKey);
		dataListRaw.remove(removeKey);

		List<List<String>> dataList = new ArrayList<>();

		String removeKey0 = keys.remove(getRandom(0, 2));
		logs.append("\n | removing removeKey0: " + removeKey0);
		dataList.add(dataListRaw.remove(removeKey0));

		String removeKey1 = keys.remove(0);
		logs.append("\n | removing removeKey1: " + removeKey1);
		dataList.add(dataListRaw.remove(removeKey1));

		int listIndex0 = getRandom(0, dataList.get(0).size() - 1);
		logs.append("\n | listIndex0: " + listIndex0);
		int listIndex1 = getRandom(0, dataList.get(1).size() - 1);
		logs.append("\n | listIndex1: " + listIndex1);

		StringBuilder strb = new StringBuilder();
		strb.append(dataList.get(0).get(listIndex0));
		strb.append(" ");
		strb.append(dataList.get(1).get(listIndex1));

		if (showLogs) {
			String[] tmp = strb.toString().split(" ");
			logs.append("\n | final: " + strb.toString());
			logs.append("\n | #words: " + tmp.length);
			System.out.println(logs);
		}

		return strb.toString();
	}

	public static int getRandom(int minimum, int maximum) {
		// int minimum = 1;
		// int maximum = 3;
		int randomNum = minimum + (int) (Math.random() * maximum);
		return randomNum;
	}

	public static List<String> getDaysOfWeek(boolean shortName) {
		DateFormatSymbols dfs = new DateFormatSymbols();
		List<String> list = null;
		if (shortName) {
			list = new ArrayList<>(Arrays.asList(dfs.getShortWeekdays()));
		} else {
			list = new ArrayList<>(Arrays.asList(dfs.getWeekdays()));
			list.remove(0);
		}
		return list;
	}

	public static List<String> getMonths(boolean shortName) {
		DateFormatSymbols dfs = new DateFormatSymbols();
		if (shortName) {
			return new ArrayList<>(Arrays.asList(dfs.getShortMonths()));
		} else {
			return new ArrayList<>(Arrays.asList(dfs.getMonths()));
		}
	}

	public static List<String> getCountries() {
		String[] locales = Locale.getISOCountries();
		List<String> countries = new ArrayList<>();
		for (String countryCode : locales) {
			Locale obj = new Locale("", countryCode);
			if (!obj.getDisplayCountry().equals("Åland Islands")
					|| !obj.getDisplayCountry().equals("C\u00f4te d'Ivoire")) {
				/* countries.add(obj.getDisplayCountry()); */
				countries.add("United States");
				countries.add("Kenya");
				countries.add("Zimbabwe");
				countries.add("Norway");
				countries.add("Argentina");
				countries.add("China");
				countries.add("Japan");
				countries.add("India");
				countries.add("Thaiwan");
				countries.add("Canada");
				countries.add("Jordan");
			} else {
				System.out.println("removing... " + obj.getDisplayCountry());
			}
			// System.out.println("Country Code = " + obj.getCountry()
			// + ", Country Name = " + obj.getDisplayCountry());
		}
		return countries;
	}

}
